import asyncio

from fastapi import HTTPException, status

from pymsr.db.sql.repositories import IUnitOfWork
from pymsr.schemas.album import (
    AlbumDetail,
    AlbumDetailBase,
    AlbumDetailBaseInput,
    AlbumDetailBasePatchInput,
    AlbumDetailFull,
    AlbumDetailWithArtistAndTags,
)
from pymsr.schemas.song import SongDetailWithArtistsAndTags
from pymsr.services.redis_service import RedisAlbumService, RedisSongService

from .songs import SongsService


class AlbumsService:
    async def add(
        self, album: AlbumDetailBaseInput, uow: IUnitOfWork
    ) -> AlbumDetailBase:
        album_dict = album.model_dump()
        try:
            async with uow:
                db_album = await uow.albums.add(album_dict)
                await uow.commit()
                return AlbumDetailBase.model_validate(db_album, from_attributes=True)
        except:
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

    async def add_all(
        self,
        albums: list[AlbumDetail],
        uow: IUnitOfWork,
    ) -> list[AlbumDetailBase]:
        albums_dicts = [album.model_dump(exclude={"songs"}) for album in albums]
        try:
            async with uow:
                results = await uow.albums.add_all(albums_dicts)
                await uow.commit()
                return [
                    AlbumDetailBase.model_validate(album, from_attributes=True)
                    for album in results
                ]
        except:
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

    async def get_full_album(
        self,
        cid: int,
        uow: IUnitOfWork,
    ) -> AlbumDetailFull:
        """Возвращает полный обьект альбома с всеми вложеностями песен и артисов"""
        try:
            # redis_conn = RedisAlbumService()
            # album = await redis_conn.get(cid)
            # if album is None:
            async with uow:
                result = await uow.albums.get_full_album(cid)
                album = AlbumDetailFull.model_validate(result, from_attributes=True)
                # await redis_conn.add(album)
                return album
            # else:
            #     return album
        except:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Not found"
            )

    async def get_album(
        self,
        cid: int,
        uow: IUnitOfWork,
    ) -> AlbumDetailBase:
        """Возвращает полный обьект альбома с всеми вложеностями песен и артисов"""
        try:
            async with uow:
                album = await uow.albums.get(cid)
                await uow.commit()
                return AlbumDetailBase.model_validate(album, from_attributes=True)
        except:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Not found"
            )

    async def get_albums(self, uow: IUnitOfWork) -> list[AlbumDetailWithArtistAndTags]:
        """Возврыщает список альбомов без песен"""
        try:
            async with uow:
                albums = await uow.albums.get_list()
                await uow.commit()
                return [
                    AlbumDetailWithArtistAndTags.model_validate(
                        album, from_attributes=True
                    )
                    for album in albums
                ]
        except:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="server error"
            )

    async def partial_update(
        self,
        cid: int,
        album: AlbumDetailBasePatchInput,
        uow: IUnitOfWork,
    ) -> AlbumDetailBase:
        async with uow:
            try:
                stored_album_data = await uow.albums.get(cid)
            except:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
            stored_item_model = AlbumDetailBase.model_validate(
                stored_album_data, from_attributes=True
            )
            update_data = album.model_dump(exclude_unset=True)
            updated_album: dict = stored_item_model.model_copy(
                update=update_data
            ).model_dump()
            try:
                db_album = await uow.albums.partial_update(updated_album)
                await uow.commit()
                return AlbumDetailBase.model_validate(db_album, from_attributes=True)
            except:
                raise HTTPException(
                    status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail="Error with partial update",
                )

    async def delete(
        self,
        cid: int,
        uow: IUnitOfWork,
    ) -> int:
        async with uow:
            try:
                album = await uow.albums.get(cid)
            except Exception as e:
                print(e)
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
            try:
                await uow.albums.delete(album.cid)
                await uow.commit()
                return album.cid
            except Exception as e:
                print(e)
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

    async def search(
        self,
        value: str,
        uow: IUnitOfWork,
    ) -> list[AlbumDetailFull]:
        async with uow:
            try:
                results = await uow.albums.search(value)
            except Exception as ex:
                print(ex)

                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

            return [
                AlbumDetailFull.model_validate(album, from_attributes=True)
                for album in results
            ]

    async def search_test(self, value: str, uow: IUnitOfWork) -> list[AlbumDetailFull]:
        async with uow:
            try:
                results = await uow.albums.search_test(value)
                tasks = [SongsService().get(cid, uow) for cid in results]
                songs = await asyncio.gather(*tasks)
                albums_cid: set[int] = {song.album_cid for song in songs}
                tasks = [self.get_full_album(cid, uow) for cid in albums_cid]
                albums = await asyncio.gather(*tasks)
                for album in albums:
                    album.songs = []
                    for song in songs:
                        if song.album_cid == album.cid:
                            album.songs.append(song)

            except Exception as ex:
                print(ex)
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
            return albums

    async def update_redis(self, uow: IUnitOfWork):
        albums = await self.get_albums(uow)
        for album in albums:
            await RedisAlbumService().add(album)
