from fastapi import HTTPException, status

from pymsr.db.sql.repositories import IUnitOfWork
from pymsr.schemas.song import (
    SongDetail,
    SongDetailBase,
    SongDetailBaseForPatch,
    SongDetailCamelCaseInput,
    SongDetailWithArtistsAndTags,
)
from pymsr.services.redis_service import RedisSongService


class SongsService:
    async def get_songs(self, uow: IUnitOfWork) -> list[SongDetailWithArtistsAndTags]:
        try:
            async with uow:
                songs = await uow.songs.get_all()
                return [
                    SongDetailWithArtistsAndTags.model_validate(
                        song, from_attributes=True
                    )
                    for song in songs
                ]
        except Exception as ex:
            print(ex)
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

    async def get(self, cid: int, uow: IUnitOfWork) -> SongDetailWithArtistsAndTags:
        """Return one song with artists and tags"""
        try:
            # redis_conn = RedisSongService()
            # redis_result = await redis_conn.get(cid)
            # if redis_result is None:
            async with uow:
                song = await uow.songs.get(cid)
                result = SongDetailWithArtistsAndTags.model_validate(
                    song, from_attributes=True
                )
                # await redis_conn.add(result)
                return result
            # else:
            #     return redis_result
        except Exception as ex:
            print(ex)
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Song with cid {cid} not found",
            )

    async def add(self, song: SongDetailBase, uow: IUnitOfWork) -> SongDetail:
        song_dict = song.model_dump()
        try:
            async with uow:
                db_song = await uow.songs.add(song_dict)
                await uow.commit()
            return SongDetail.model_validate(db_song, from_attributes=True)
        except:
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

    async def add_all(
        self,
        songs: list[SongDetailCamelCaseInput],
        uow: IUnitOfWork,
    ) -> list[SongDetail]:
        albums_dicts = [album.model_dump(exclude={"songs"}) for album in songs]
        try:
            async with uow:
                results = await uow.songs.add_all(albums_dicts)
                await uow.commit()
        except:
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return [
            SongDetail.model_validate(song, from_attributes=True) for song in results
        ]

    async def partial_update(
        self,
        cid: int,
        songs: SongDetailBaseForPatch,
        uow: IUnitOfWork,
    ) -> SongDetail:
        async with uow:
            try:
                stored_data = await uow.songs.get(cid)
            except:
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
            stored_item_model = SongDetail.model_validate(
                stored_data, from_attributes=True
            )
            update_data = songs.model_dump(exclude_unset=True)
            updated_obj: dict = stored_item_model.model_copy(
                update=update_data
            ).model_dump()
            try:
                db_obj = await uow.songs.partial_update(updated_obj)
                await uow.commit()
                return SongDetail.model_validate(db_obj, from_attributes=True)
            except:
                raise HTTPException(
                    status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                    detail="Error with partial update",
                )

    async def delete(
        self,
        cid: int,
        uow: IUnitOfWork,
    ) -> int:
        async with uow:
            try:
                obj = await uow.songs.get(cid)
            except Exception as e:
                print(e)
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
            try:
                await uow.songs.delete(obj.cid)
                await uow.commit()
                return obj.cid
            except Exception as e:
                print(e)
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR)

    async def update_redis(self, uow: IUnitOfWork):
        songs = await self.get_songs(uow)
        for song in songs:
            await RedisSongService().add(song)
