from pymsr.db.redis.repositories import (RedisAlbumRepository, RedisRepository,
                                         RedisSongsRepository)
from pymsr.db.sql.repositories.unitofwork import UnitOfWork
from pymsr.schemas.album import AlbumDetailBase, AlbumDetailFull
from pymsr.schemas.song import (SongDetail, SongDetailBase,
                                SongDetailWithArtistsAndTags)


class RedisAlbumService:
    async def add(self, album: AlbumDetailBase):
        data = {"key": album.cid, "value": album.model_dump_json()}
        await RedisAlbumRepository().add(data)

    async def get(self, cid: int) -> AlbumDetailFull | None:
        try:
            result = await RedisAlbumRepository().get(cid)
            if result is None:
                return None
            return AlbumDetailFull.model_validate_json(result)
        except Exception as ex:
            print(ex)
            raise

    async def update(self, album: AlbumDetailBase):
        await self.add(album)

    async def delete(self, cid: int):
        await RedisAlbumRepository().delete(cid)


class RedisSongService:
    async def add(self, song: SongDetailWithArtistsAndTags):
        data = {"key": song.cid, "value": song.model_dump_json()}
        # print(data)
        await RedisSongsRepository().add(data)

    async def get(self, cid: int) -> SongDetailWithArtistsAndTags | None:
        try:
            result = await RedisSongsRepository().get(cid)
            if result is None:
                return None
            else:
                return SongDetailWithArtistsAndTags.model_validate_json(result)
        except Exception as ex:
            print(ex)
            raise

    async def update(self, song: SongDetailWithArtistsAndTags):
        await self.add(song)

    async def delete(self, cid: int):
        await RedisSongsRepository().delete(cid)
