from email.message import EmailMessage

import aiosmtplib

from pymsr.common.settings import settings


class EmailService:
    async def send_mail(self, to: str, subject: str, content: str):
        message = EmailMessage()
        message["From"] = settings.env.SMTP_USERNAME
        message["To"] = to
        message["Subject"] = subject
        message.set_content(content)

        # result: tuple[dict[str, aiosmtplib.SMTPResponse], str] = await aiosmtplib.send(
        await aiosmtplib.send(
            message,
            hostname=settings.env.SMTP_HOSTNAME,
            port=settings.env.SMTP_PORT,
            username=settings.env.SMTP_USERNAME,
            password=settings.env.SMTP_PASSWORD,
            use_tls=settings.env.SMTP_USE_TLS,
        )
