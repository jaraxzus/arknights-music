from abc import ABC, abstractmethod

from fastapi import HTTPException, status
from pydantic import BaseModel
from sqlalchemy.exc import (AmbiguousForeignKeysError, IntegrityError,
                            NoResultFound)

from pymsr.db.sql.models import User
from pymsr.db.sql.repositories import (IUnitOfWork, OfferAlbumArtistReposytory,
                                       OfferAlbumTagReposytory,
                                       OfferSongArtistReposytory,
                                       OfferSongTagReposytory,
                                       SQLAlchemyOfferRepository)
from pymsr.schemas import (AlbumArtistOffer, AlbumArtistOfferBase,
                           AlbumTagOffer, AlbumTagOfferBase, SongArtistOffer,
                           SongArtistOfferBase, SongTagOffer, SongTagOfferBase)


class AbstractOfferService(ABC):
    input_schema: type[BaseModel] | None = None
    output_schema: type[BaseModel] | None = None
    reposytory: type[SQLAlchemyOfferRepository] | None = None

    async def get_offer(self, user: User, cid: int, uow: IUnitOfWork):
        if (
            self.output_schema is None
            or self.output_schema is None
            or self.reposytory is None
        ):
            raise ValueError("Ivalid service params")
        try:
            async with uow:
                offer = await self.reposytory(uow.session).get(cid)
                return self.output_schema.model_validate(
                    offer.__dict__, from_attributes=True
                )
        except NoResultFound:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
            )
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Error processing geting offer",
            ) from e

    async def get_all_offers(self, uow: IUnitOfWork):
        if (
            self.output_schema is None
            or self.output_schema is None
            or self.reposytory is None
        ):
            raise ValueError("Ivalid service params")
        try:
            async with uow:
                offers = await self.reposytory(uow.session).get_list()
                return [
                    self.output_schema.model_validate(offer, from_attributes=True)
                    for offer in offers
                ]
        except NoResultFound:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="One of relation not exist",
            )
        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Error processing relation operation",
            ) from e

    async def create_offer(
        self,
        user: User,
        offer: BaseModel,
        uow: IUnitOfWork,
    ):
        if (
            self.output_schema is None
            or self.output_schema is None
            or self.reposytory is None
        ):
            raise ValueError("Ivalid service params")
        try:
            async with uow:
                offer_dict = offer.model_dump()
                offer_dict["user_id"] = user.id
                result = await self.reposytory(uow.session).add(offer_dict)
                await uow.commit()
                return self.output_schema.model_validate(
                    result.__dict__, from_attributes=True
                )
        except IntegrityError as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail="ivalid oject cid",
            ) from e
        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            ) from e

    @abstractmethod
    async def approve_offer(self, cid: int, uow: IUnitOfWork):
        raise NotImplementedError

    async def delete_offer(
        self,
        user: User,
        cid: int,
        uow: IUnitOfWork,
    ) -> None:
        if (
            self.output_schema is None
            or self.output_schema is None
            or self.reposytory is None
        ):
            raise ValueError("Ivalid service params")
        try:
            async with uow:
                reposytory = self.reposytory(uow.session)
                offer = await reposytory.get(cid)
                if user.is_superuser or offer.user_id == user.id:
                    result = await reposytory.delete(cid)
                    await uow.commit()

        except NoResultFound:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
            )

        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            ) from e


class OfferAlbumArtistService(AbstractOfferService):
    input_schema = AlbumArtistOfferBase
    output_schema = AlbumArtistOffer
    reposytory = OfferAlbumArtistReposytory

    async def approve_offer(self, cid: int, uow: IUnitOfWork):
        if (
            self.output_schema is None
            or self.output_schema is None
            or self.reposytory is None
        ):
            raise ValueError("Ivalid service params")
        try:
            async with uow:
                offer = await uow.offer_album_artist.get(cid)
                if offer.operation == "ADD":
                    if offer.artist_cid is None:
                        # проверка по имени
                        artist = await uow.artists.get_by_name(offer.artist)
                        if artist is None:
                            artist = await uow.artists.add({"name": offer.artist})
                    else:
                        artist = await uow.artists.get(offer.artist_cid)

                    await uow.album_artist.add(
                        {"album_cid": offer.album_cid, "artist_cid": artist.cid}
                    )
                else:
                    await uow.album_artist.delete(
                        {"album_cid": offer.album_cid, "artist_cid": offer.artist_cid}
                    )
                await uow.offer_album_artist.delete(offer.cid)
                await uow.commit()

        except NoResultFound:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Inavlid offer"
            )
        except IntegrityError as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail="Already exists, the offer is not valid",
            ) from e

        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            ) from e


class OfferAlbumTagService(AbstractOfferService):
    input_schema = AlbumTagOfferBase
    output_schema = AlbumTagOffer
    reposytory = OfferAlbumTagReposytory

    async def approve_offer(self, cid: int, uow: IUnitOfWork):
        if (
            self.output_schema is None
            or self.output_schema is None
            or self.reposytory is None
        ):
            raise ValueError("Ivalid service params")
        try:
            async with uow:
                offer = await uow.offer_album_tag.get(cid)
                if offer.operation == "ADD":
                    if offer.tag_cid is None:
                        tag = await uow.tags.get_by_tag(offer.tag)
                        if tag is None:
                            tag = await uow.tags.add({"tag": offer.tag})
                    else:
                        tag = await uow.tags.get(offer.tag_cid)

                    await uow.album_tag.add(
                        {"album_cid": offer.album_cid, "tag_cid": tag.cid}
                    )
                else:
                    await uow.album_tag.delete(
                        {"album_cid": offer.album_cid, "tag_cid": offer.tag_cid}
                    )

                await uow.offer_album_tag.delete(offer.cid)
                await uow.commit()

        except NoResultFound:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Inavlid offer"
            )

        except IntegrityError as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail="Already exists, the offer is not valid",
            ) from e
        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            ) from e


class OfferSongArtistService(AbstractOfferService):
    input_schema = SongArtistOfferBase
    output_schema = SongArtistOffer
    reposytory = OfferSongArtistReposytory

    async def approve_offer(self, cid: int, uow: IUnitOfWork):
        if (
            self.output_schema is None
            or self.output_schema is None
            or self.reposytory is None
        ):
            raise ValueError("Ivalid service params")
        try:
            async with uow:
                offer = await uow.offer_song_artist.get(cid)
                if offer.operation == "ADD":
                    if offer.artist_cid is None:
                        artist = await uow.artists.get_by_name(offer.artist)
                        if artist is None:
                            artist = await uow.artists.add({"name": offer.artist})
                    else:
                        artist = await uow.artists.get(offer.artist_cid)

                    await uow.song_artist.add(
                        {"song_cid": offer.song_cid, "artist_cid": artist.cid}
                    )
                else:
                    await uow.song_artist.delete(
                        {"song_cid": offer.song_cid, "artist_cid": offer.artist_cid}
                    )

                await uow.offer_song_artist.delete(offer.cid)
                await uow.commit()

        except NoResultFound:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Inavlid offer"
            )

        except IntegrityError as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail="Already exists, the offer is not valid",
            ) from e
        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            ) from e


class OfferSongTagService(AbstractOfferService):
    input_schema = SongTagOfferBase
    output_schema = SongTagOffer
    reposytory = OfferSongTagReposytory

    async def approve_offer(self, cid: int, uow: IUnitOfWork):
        if (
            self.output_schema is None
            or self.output_schema is None
            or self.reposytory is None
        ):
            raise ValueError("Ivalid service params")
        try:
            async with uow:
                offer = await uow.offer_song_tag.get(cid)
                if offer.operation == "ADD":
                    if offer.tag_cid is None:
                        tag = await uow.tags.get_by_tag(offer.tag)
                        if tag is None:
                            tag = await uow.tags.add({"tag": offer.tag})
                    else:
                        tag = await uow.tags.get(offer.tag_cid)

                    await uow.song_tag.add(
                        {"song_cid": offer.song_cid, "tag_cid": tag.cid}
                    )
                else:
                    await uow.song_tag.delete(
                        {"song_cid": offer.song_cid, "tag_cid": offer.tag_cid}
                    )
                await uow.offer_song_tag.delete(offer.cid)
                await uow.commit()

        except NoResultFound:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Inavlid offer"
            )
        except IntegrityError as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail="Already exists, the offer is not valid",
            ) from e
        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            ) from e
