import asyncio
import sys

from pymsr.common.msr_client import MSRClient
from pymsr.common.settings import settings
from pymsr.db.sql.models.song import Song
from pymsr.db.sql.repositories.unitofwork import IUnitOfWork, UnitOfWork
from pymsr.schemas.album import AlbumDetail, AlbumDetailFull, AlbumInput
from pymsr.schemas.song import SongDetailCamelCaseInput

sys.path.append("...")


async def update_url(
    song: Song,
    msr: MSRClient,
):
    result = await msr.get_song(song.cid)
    print(f"new url {result.source_url}")
    song.source_url = result.source_url


class UpdateService:
    def __init__(self) -> None:
        self.uow: IUnitOfWork = UnitOfWork()

    async def update_sounds_url(self):
        async with MSRClient() as msr:
            async with self.uow:
                songs = await self.uow.songs.get_all()
                await asyncio.gather(*[update_url(song, msr) for song in songs])
                await self.uow.commit()

    async def update(
        self,
    ) -> list[AlbumDetailFull]:
        # print(settings.env.MAIN_URL)
        async with MSRClient() as msr:
            async with self.uow:
                msr_albums = await msr.get_albums()
                on_db_albums = await self.uow.albums.get_list()
                on_db_albums_cids = set(obj.cid for obj in on_db_albums)
                new_albums = [
                    obj for obj in msr_albums if obj.cid not in on_db_albums_cids
                ]
                tasks = []
            for album in new_albums:
                tasks.append(self._add_album(album, msr))

            return await asyncio.gather(*tasks)

    async def _add_album(self, album: AlbumInput, msr: MSRClient):
        full_album: AlbumDetail = await msr.get_album(album.cid)
        try:
            await msr.download_cover(
                url=full_album.cover_url,
                name="cover",
                path=f"/usr/src/app/albums/{album.name}",
            )
        except Exception as ex:
            print(ex)
        full_album.cover_url = f"{settings.env.MAIN_URL}albums/{full_album.name}/cover.{full_album.cover_url.split('.')[-1]}"
        albums_dicts = full_album.model_dump(exclude={"songs"})
        uow = UnitOfWork()
        try:
            async with uow:
                new_on_db_album = await uow.albums.add(albums_dicts)
                # созадние и привязка артистов
                for artist in album.artistes:
                    on_db_artist = await uow.artists.get_by_name(artist)
                    if on_db_artist is None:
                        on_db_artist = await uow.artists.add({"name": artist})
                    await uow.album_artist.add(
                        {
                            "album_cid": new_on_db_album.cid,
                            "artist_cid": on_db_artist.cid,
                        }
                    )
                # загрузка детальной информации по саундам
                tasks = [msr.get_song(song.cid) for song in full_album.songs]
                songs: list[SongDetailCamelCaseInput] = await asyncio.gather(*tasks)

                for song in songs:
                    # добавление трека в бд исключая артисов
                    new_on_db_song = await uow.songs.add(
                        song.model_dump(exclude={"artists"})
                    )
                    # проверка на существование или добавлние артиста
                    for artist in song.artists:
                        on_db_artist = await uow.artists.get_by_name(artist)
                        if on_db_artist is None:
                            on_db_artist = await uow.artists.add({"name": artist})
                        # связывание трека с артистом
                        await uow.song_artist.add(
                            {
                                "song_cid": new_on_db_song.cid,
                                "artist_cid": on_db_artist.cid,
                            }
                        )
                await uow.commit()
                result = await uow.albums.get_full_album(new_on_db_album.cid)
                return AlbumDetailFull.model_validate(result, from_attributes=True)

        except Exception as ex:
            print(ex)
