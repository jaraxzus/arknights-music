from fastapi import HTTPException, status

from pymsr.db.sql.repositories import IUnitOfWork
from pymsr.schemas import Tag, TagBase


class TagsService:
    async def add(
        self,
        tag: TagBase,
        uow: IUnitOfWork,
    ) -> Tag:
        try:
            async with uow:
                db_tag = tag.model_dump()
                result = await uow.tags.add(db_tag)
                await uow.commit()
                return Tag.model_validate(result, from_attributes=True)
        except:
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT, detail=f"{tag.tag} already exists"
            )

    async def get_tags(self, uow: IUnitOfWork) -> list[Tag]:
        try:
            async with uow:
                db_tags = await uow.tags.get_list()
                return [
                    Tag.model_validate(tag, from_attributes=True) for tag in db_tags
                ]
        except:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Not found"
            )

    async def get(self, cid: int, uow: IUnitOfWork) -> Tag:
        try:
            async with uow:
                tag = await uow.tags.get(cid)
                return Tag.model_validate(tag, from_attributes=True)
        except Exception as ex:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Not found"
            )

    async def update(self, tag: Tag, uow: IUnitOfWork) -> Tag:
        async with uow:
            try:
                stored_tag = await uow.tags.get(tag.cid)
            except:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND, detail="Not found"
                )
            db_tag = Tag.model_validate(stored_tag, from_attributes=True)
            db_tag.tag = tag.tag
            result = await uow.tags.partial_update(db_tag.model_dump())
            await uow.commit()
            return Tag.model_validate(result, from_attributes=True)

    async def delete(self, cid: int, uow: IUnitOfWork):
        async with uow:
            try:
                tag = uow.tags.get(cid)
                await uow.tags.delete(cid)
                await uow.commit()
            except:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND, detail="Not found"
                )
