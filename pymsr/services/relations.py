from typing import Callable

from fastapi import HTTPException, status
from fastapi_cache import FastAPICache
from sqlalchemy.exc import AmbiguousForeignKeysError, IntegrityError

from pymsr.api.v1.routers.enums import CacheNameSpace
from pymsr.db.sql.repositories import IUnitOfWork
from pymsr.schemas import (
    AlbumArtist,
    AlbumArtistBase,
    AlbumTag,
    AlbumTagBase,
    SongArtist,
    SongArtistBase,
    SongTag,
    SongTagBase,
)


class RelationService:
    async def _process_relation_operation(
        self,
        relation,
        uow: IUnitOfWork,
        operation_func: Callable,
        error_detail: str,
    ):
        try:
            match relation:
                case AlbumArtistBase():
                    await uow.albums.get(relation.album_cid)
                    await uow.artists.get(relation.artist_cid)
                case AlbumTagBase():
                    await uow.albums.get(relation.album_cid)
                    await uow.tags.get(relation.tag_cid)
                case SongTagBase():
                    await uow.songs.get(relation.song_cid)
                    await uow.tags.get(relation.tag_cid)
                case SongArtistBase():
                    await uow.songs.get(relation.song_cid)
                    await uow.artists.get(relation.artist_cid)
                case _:
                    raise ValueError("Error input value")

        except AmbiguousForeignKeysError as e:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="One of relation not exist",
            ) from e
        except IntegrityError as e:
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail=error_detail,
            ) from e
        except Exception as e:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Error processing relation operation",
            ) from e
        result = await operation_func(relation.model_dump())
        await uow.commit()
        await FastAPICache.clear(CacheNameSpace.ALBUMS.value)
        return result

    async def create_album_artist_relation(
        self,
        relation: AlbumArtistBase,
        uow: IUnitOfWork,
    ) -> AlbumArtist:
        async with uow:
            result = await self._process_relation_operation(
                relation, uow, uow.album_artist.add, "Relation already exists"
            )
            return AlbumArtist.model_validate(result, from_attributes=True)

    async def delete_album_artist_relation(
        self,
        relation: AlbumArtistBase,
        uow: IUnitOfWork,
    ) -> None:
        async with uow:
            result = await self._process_relation_operation(
                relation, uow, uow.album_artist.delete, "Relation not found"
            )

    async def create_album_tag_relation(
        self,
        relation: AlbumTagBase,
        uow: IUnitOfWork,
    ) -> AlbumTag:
        async with uow:
            result = await self._process_relation_operation(
                relation, uow, uow.album_tag.add, "Relation already exists"
            )
            return AlbumTag.model_validate(result, from_attributes=True)

    async def delete_album_tag_relation(
        self,
        relation: AlbumTagBase,
        uow: IUnitOfWork,
    ) -> None:
        async with uow:
            result = await self._process_relation_operation(
                relation, uow, uow.album_tag.delete, "Relation not found"
            )

    async def create_song_artist_relation(
        self,
        relation: SongArtistBase,
        uow: IUnitOfWork,
    ) -> SongArtist:
        async with uow:
            result = await self._process_relation_operation(
                relation, uow, uow.song_artist.add, "Relation already exists"
            )
            return SongArtist.model_validate(result, from_attributes=True)

    async def delete_song_artist_relation(
        self,
        relation: SongArtistBase,
        uow: IUnitOfWork,
    ) -> None:
        async with uow:
            result = await self._process_relation_operation(
                relation, uow, uow.song_artist.delete, "Relation not found"
            )

    async def create_song_tag_relaton(
        self,
        relation: SongTagBase,
        uow: IUnitOfWork,
    ) -> SongTag:
        async with uow:
            result = await self._process_relation_operation(
                relation, uow, uow.song_tag.add, "Relation already exists"
            )
            return SongTag.model_validate(result, from_attributes=True)

    async def delete_song_tag_relation(
        self,
        relation: SongTagBase,
        uow: IUnitOfWork,
    ) -> None:
        async with uow:
            result = await self._process_relation_operation(
                relation, uow, uow.song_tag.delete, "Relation not found"
            )
