from fastapi import HTTPException, status

from pymsr.db.sql.repositories import IUnitOfWork
from pymsr.schemas import Artist, BaseArtist


class ArtistsService:
    async def add(
        self,
        artist: BaseArtist,
        uow: IUnitOfWork,
    ) -> Artist:
        try:
            async with uow:
                db_artist = artist.model_dump()
                result = await uow.artists.add(db_artist)
                await uow.commit()
                return Artist.model_validate(result, from_attributes=True)
        except:
            raise HTTPException(
                status_code=status.HTTP_409_CONFLICT,
                detail=f"{artist.name} already exists",
            )

    async def get_artists(self, uow: IUnitOfWork) -> list[Artist]:
        try:
            async with uow:
                db_artists = await uow.artists.get_list()
                return [
                    Artist.model_validate(artist, from_attributes=True)
                    for artist in db_artists
                ]
        except:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Not found"
            )

    async def get(self, cid: int, uow: IUnitOfWork) -> Artist:
        try:
            async with uow:
                artist = await uow.artists.get(cid)
                return Artist.model_validate(artist, from_attributes=True)
        except Exception as ex:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND, detail="Not found"
            )

    async def update(self, artist: Artist, uow: IUnitOfWork) -> Artist:
        async with uow:
            try:
                stored_artist = await uow.artists.get(artist.cid)
            except:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND, detail="Not found"
                )
            db_artist = Artist.model_validate(stored_artist, from_attributes=True)
            db_artist.name = artist.name
            result = await uow.artists.partial_update(db_artist.model_dump())
            await uow.commit()
            return Artist.model_validate(result, from_attributes=True)

    async def delete(self, cid: int, uow: IUnitOfWork):
        async with uow:
            try:
                artist = uow.artists.get(cid)
                await uow.artists.delete(cid)
                await uow.commit()
            except:
                raise HTTPException(
                    status_code=status.HTTP_404_NOT_FOUND, detail="Not found"
                )
