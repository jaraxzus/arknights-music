from typing import Annotated

from fastapi import Depends

from pymsr.db.sql.repositories.unitofwork import IUnitOfWork, UnitOfWork

UOWDep = Annotated[IUnitOfWork, Depends(UnitOfWork)]
