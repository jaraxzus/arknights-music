from fastapi import APIRouter, Depends, status
from fastapi_cache.decorator import cache

from pymsr.api.v1.dependencies import UOWDep
from pymsr.auth.users import current_super_user
from pymsr.db.sql.models.auth_models import User
from pymsr.schemas.album import (AlbumDetailBase, AlbumDetailBaseInput,
                                 AlbumDetailBasePatchInput, AlbumDetailFull,
                                 AlbumDetailWithArtistAndTags)
from pymsr.services.albums import AlbumsService

from .enums import CacheNameSpace

router = APIRouter(
    prefix="/albums",
    tags=["albums"],
)


@router.get("/", status_code=status.HTTP_200_OK)
@cache(expire=20, namespace=CacheNameSpace.ALBUMS.value)
async def get_albums(
    uow: UOWDep,
) -> list[AlbumDetailWithArtistAndTags]:
    """Return a list of albums without songs"""
    return await AlbumsService().get_albums(uow)


@router.post("/", status_code=status.HTTP_201_CREATED)
async def add_album(
    album: AlbumDetailBaseInput,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> AlbumDetailBase:
    """Add album"""
    return await AlbumsService().add(album, uow)


@router.get("/{cid}/", status_code=status.HTTP_200_OK)
async def get_album(
    cid: int,
    uow: UOWDep,
) -> AlbumDetailFull:
    """Accepts the cid of the album and returns the full album object with a list of songs"""
    return await AlbumsService().get_full_album(cid, uow)


@router.patch("/{cid}/", status_code=status.HTTP_200_OK)
async def patch_alubm(
    cid: int,
    album: AlbumDetailBasePatchInput,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> AlbumDetailBaseInput:
    """Partial album data update"""
    return await AlbumsService().partial_update(cid, album, uow)


@router.delete("/{cid}/", status_code=status.HTTP_200_OK)
async def delete_album(
    cid: int,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> int:
    """Delete albums"""
    return await AlbumsService().delete(cid, uow)
