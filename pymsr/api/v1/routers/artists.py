from fastapi import APIRouter, Depends, HTTPException, Response, status

from pymsr.api.v1.dependencies import UOWDep
from pymsr.auth.users import current_super_user
from pymsr.db.sql.models.auth_models import User
from pymsr.schemas.artist import Artist, BaseArtist
from pymsr.services.artists import ArtistsService

router = APIRouter(
    prefix="/artists",
    tags=["artist"],
)


@router.get("/", status_code=status.HTTP_200_OK)
async def get_artists(
    uow: UOWDep,
) -> list[Artist]:
    return await ArtistsService().get_artists(uow)


@router.get("/{cid}/", status_code=status.HTTP_200_OK)
async def get_artist(
    cid: int,
    uow: UOWDep,
) -> Artist:
    """Returns artist with albums and"""
    return await ArtistsService().get(cid, uow)


@router.post("/", status_code=status.HTTP_201_CREATED)
async def add_artist(
    artist: BaseArtist,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Artist:
    return await ArtistsService().add(artist, uow)


@router.put("/{cid}/", status_code=status.HTTP_200_OK)
async def patch_artist(
    cid: int,
    artist: Artist,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Artist:
    if cid != artist.cid:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"cid in Path != cid in obj",
        )
    return await ArtistsService().update(artist, uow)


@router.delete("/{cid}/", status_code=status.HTTP_200_OK)
async def delete_artist(
    cid: int,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Response:
    await ArtistsService().delete(cid, uow)
    return Response("Ok!")
