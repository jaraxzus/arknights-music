from fastapi import APIRouter, status

from pymsr.api.v1.dependencies import UOWDep
from pymsr.schemas.album import AlbumDetailFull
from pymsr.services.albums import AlbumsService
from pymsr.services.redis_service import RedisAlbumService, RedisSongService

router = APIRouter(
    prefix="/search",
    tags=["search"],
)


@router.get("/", status_code=status.HTTP_200_OK)
async def search(
    value: str,
    uow: UOWDep,
) -> list[AlbumDetailFull]:
    return await AlbumsService().search(value, uow)


@router.get("/test", status_code=status.HTTP_200_OK)
async def test_search(
    value: str,
    uow: UOWDep,
) -> list[AlbumDetailFull]:
    return await AlbumsService().search_test(value, uow)
