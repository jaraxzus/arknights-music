from fastapi import APIRouter, Depends, HTTPException, Response, status

from pymsr.api.v1.dependencies import UOWDep
from pymsr.auth.users import current_super_user
from pymsr.db.sql.models.auth_models import User
from pymsr.schemas import Tag, TagBase
from pymsr.services.tags import TagsService

router = APIRouter(
    prefix="/tags",
    tags=["tags"],
)


@router.get("/", status_code=status.HTTP_200_OK)
async def get_tags(
    uow: UOWDep,
) -> list[Tag]:
    return await TagsService().get_tags(uow)


@router.get("/{cid}/", status_code=status.HTTP_200_OK)
async def get_tag(
    cid: int,
    uow: UOWDep,
) -> Tag:
    return await TagsService().get(cid, uow)


@router.post("/", status_code=status.HTTP_201_CREATED)
async def add(
    tag: TagBase,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Tag:
    return await TagsService().add(tag, uow)


@router.put("/{cid}/", status_code=status.HTTP_200_OK)
async def patch(
    cid: int,
    tag: Tag,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Tag:
    if cid != tag.cid:
        raise HTTPException(
            status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
            detail=f"cid in Path != cid in obj",
        )
    return await TagsService().update(tag, uow)


@router.delete("/{cid}/", status_code=status.HTTP_200_OK)
async def delete(
    cid: int,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Response:
    await TagsService().delete(cid, uow)
    return Response("Ok!")
