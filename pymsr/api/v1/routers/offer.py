from fastapi import APIRouter, Depends, HTTPException, Response, status
from pydantic import BaseModel

from pymsr.api.v1.dependencies import UOWDep
from pymsr.auth.users import current_super_user, current_verified_user
from pymsr.db.sql.models import User
from pymsr.schemas import (AlbumArtistOffer, AlbumArtistOfferBase,
                           AlbumTagOffer, AlbumTagOfferBase, SongArtistOffer,
                           SongArtistOfferBase, SongTagOffer, SongTagOfferBase)
from pymsr.services import (OfferAlbumArtistService, OfferAlbumTagService,
                            OfferSongArtistService, OfferSongTagService)

router = APIRouter(
    prefix="/offer",
    tags=["offer"],
)


class Confirm(BaseModel):
    confirm: bool


@router.get("/album-artist/{cid}/", status_code=status.HTTP_200_OK)
async def get_album_artits_offer(
    cid: int, uow: UOWDep, user: User = Depends(current_verified_user)
) -> AlbumArtistOffer:
    return await OfferAlbumArtistService().get_offer(user, cid, uow)


@router.post("/album-artist/{cid}/", status_code=status.HTTP_200_OK)
async def approve_album_artits_offer(
    cid: int,
    confirm: Confirm,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Response:
    if confirm.confirm:
        await OfferAlbumArtistService().approve_offer(cid, uow)
        return Response("Ok!")
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)


@router.get("/album-artist/")
async def get_album_artits_offer_list(
    uow: UOWDep,
) -> list[AlbumArtistOffer]:
    return await OfferAlbumArtistService().get_all_offers(uow)


@router.post("/album-artist/", status_code=status.HTTP_201_CREATED)
async def create_album_artist_offer(
    offer: AlbumArtistOfferBase,
    uow: UOWDep,
    user: User = Depends(current_verified_user),
) -> AlbumArtistOffer:
    return await OfferAlbumArtistService().create_offer(user, offer, uow)


@router.delete("/album-artist/", status_code=status.HTTP_200_OK)
async def delete_album_artist_offer(
    cid: int,
    uow: UOWDep,
    user: User = Depends(current_verified_user),
) -> Response:
    await OfferAlbumArtistService().delete_offer(user, cid, uow)
    return Response("Ok!")


@router.get("/album-tag/{cid}/", status_code=status.HTTP_200_OK)
async def get_album_tag_offer(
    cid: int, uow: UOWDep, user: User = Depends(current_verified_user)
) -> AlbumTagOffer:
    return await OfferAlbumTagService().get_offer(user, cid, uow)


@router.post("/album-tag/{cid}/", status_code=status.HTTP_200_OK)
async def approve_album_tag_offer(
    cid: int,
    confirm: Confirm,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Response:
    if confirm.confirm:
        await OfferAlbumTagService().approve_offer(cid, uow)
        return Response("Ok!")
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)


@router.get("/album-tag/")
async def get_album_tag_offer_list(
    uow: UOWDep,
) -> list[AlbumTagOffer]:
    return await OfferAlbumTagService().get_all_offers(uow)


@router.post("/album-tag/", status_code=status.HTTP_201_CREATED)
async def create_album_tag_offer(
    offer: AlbumTagOfferBase,
    uow: UOWDep,
    user: User = Depends(current_verified_user),
) -> AlbumTagOffer:
    return await OfferAlbumTagService().create_offer(user, offer, uow)


@router.delete("/album-tag/", status_code=status.HTTP_200_OK)
async def delete_album_tag_offer(
    cid: int,
    uow: UOWDep,
    user: User = Depends(current_verified_user),
) -> Response:
    await OfferAlbumTagService().delete_offer(user, cid, uow)
    return Response("Ok!")


@router.get("/song-artist/{cid}/", status_code=status.HTTP_200_OK)
async def get_song_artist_offer(
    cid: int, uow: UOWDep, user: User = Depends(current_verified_user)
) -> SongArtistOffer:
    return await OfferSongArtistService().get_offer(user, cid, uow)


@router.post("/song-artist/{cid}/", status_code=status.HTTP_200_OK)
async def approve_song_artist_offer(
    cid: int,
    confirm: Confirm,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Response:
    if confirm.confirm:
        await OfferSongArtistService().approve_offer(cid, uow)
        return Response("Ok!")
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)


@router.get("/song-artist/")
async def get_song_artist_offer_list(
    uow: UOWDep,
) -> list[SongArtistOffer]:
    return await OfferSongArtistService().get_all_offers(uow)


@router.post("/song-artist/", status_code=status.HTTP_201_CREATED)
async def create_song_artist_offer(
    offer: SongArtistOfferBase,
    uow: UOWDep,
    user: User = Depends(current_verified_user),
) -> SongArtistOffer:
    return await OfferSongArtistService().create_offer(user, offer, uow)


@router.delete("/song-artist/", status_code=status.HTTP_200_OK)
async def delete_song_artist_offer(
    cid: int,
    uow: UOWDep,
    user: User = Depends(current_verified_user),
) -> Response:
    await OfferSongArtistService().delete_offer(user, cid, uow)
    return Response("Ok!")


@router.get("/song-tag/{cid}/", status_code=status.HTTP_200_OK)
async def get_song_tag_offer(
    cid: int, uow: UOWDep, user: User = Depends(current_verified_user)
) -> SongTagOffer:
    return await OfferSongTagService().get_offer(user, cid, uow)


@router.post("/song-tag/{cid}/", status_code=status.HTTP_200_OK)
async def approve_song_tag_offer(
    cid: int,
    confirm: Confirm,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Response:
    if confirm.confirm:
        await OfferSongTagService().approve_offer(cid, uow)
        return Response("Ok!")
    else:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)


@router.get("/song-tag/")
async def get_song_tag_offer_list(
    uow: UOWDep,
) -> list[SongTagOffer]:
    return await OfferSongTagService().get_all_offers(uow)


@router.post("/song-tag/", status_code=status.HTTP_201_CREATED)
async def create_song_tag_relaton(
    offer: SongTagOfferBase,
    uow: UOWDep,
    user: User = Depends(current_verified_user),
) -> SongTagOffer:
    return await OfferSongTagService().create_offer(user, offer, uow)


@router.delete("/song-tag/", status_code=status.HTTP_200_OK)
async def delete_song_tag_offer(
    cid: int,
    uow: UOWDep,
    user: User = Depends(current_verified_user),
) -> Response:
    await OfferSongTagService().delete_offer(user, cid, uow)
    return Response("Ok!")
