from fastapi import APIRouter, Depends, status

from pymsr.api.v1.dependencies import UOWDep
from pymsr.auth.users import current_super_user
from pymsr.db.sql.models.auth_models import User
from pymsr.schemas.song import (
    SongDetail,
    SongDetailBase,
    SongDetailBaseForPatch,
    SongDetailWithArtistsAndTags,
)
from pymsr.services.songs import SongsService

router = APIRouter(
    prefix="/song",
    tags=["song"],
)


@router.get("/{cid}/", status_code=status.HTTP_200_OK)
async def get_song(
    cid: int,
    uow: UOWDep,
) -> SongDetailWithArtistsAndTags:
    """Return full song"""
    return await SongsService().get(cid, uow)


@router.post("/", status_code=status.HTTP_201_CREATED)
async def add_song(
    song: SongDetailBase,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> SongDetail:
    return await SongsService().add(song, uow)


@router.patch("/{cid}/", status_code=status.HTTP_200_OK)
async def patch_song(
    cid: int,
    uow: UOWDep,
    song: SongDetailBaseForPatch,
    user: User = Depends(current_super_user),
) -> SongDetail:
    return await SongsService().partial_update(cid, song, uow)


@router.delete("/{cid}/", status_code=status.HTTP_200_OK)
async def delete_song(
    cid: int,
    uow: UOWDep,
    user: User = Depends(current_super_user),
):
    return await SongsService().delete(cid, uow)
