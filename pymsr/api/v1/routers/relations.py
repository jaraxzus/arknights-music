from fastapi import APIRouter, Depends, Response, status
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from fastapi_cache.decorator import cache

from pymsr.api.v1.dependencies import UOWDep
from pymsr.auth.users import current_super_user
from pymsr.db.sql.models.auth_models import User
from pymsr.schemas import (
    AlbumArtist,
    AlbumArtistBase,
    AlbumTag,
    AlbumTagBase,
    SongArtist,
    SongArtistBase,
)
from pymsr.schemas.song_tag_relation import SongTag, SongTagBase
from pymsr.services.relations import RelationService

from .enums import CacheNameSpace

router = APIRouter(
    prefix="/relations",
    tags=["relations"],
)


@router.post("/album-artist/", status_code=status.HTTP_201_CREATED)
async def create_album_artist_relation(
    relation: AlbumArtistBase,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> AlbumArtist:
    return await RelationService().create_album_artist_relation(relation, uow)


@router.delete("/album-artist/", status_code=status.HTTP_200_OK)
async def delete_album_artist_relation(
    album_cid: int,
    artist_cid: int,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Response:
    relation = AlbumArtistBase(album_cid=album_cid, artist_cid=artist_cid)
    await RelationService().delete_album_artist_relation(relation, uow)
    return Response("Ok!")


@router.post("/album-tag/", status_code=status.HTTP_201_CREATED)
async def create_album_tag_relation(
    relation: AlbumTagBase,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> AlbumTag:
    return await RelationService().create_album_tag_relation(relation, uow)


@router.delete("/album-tag/", status_code=status.HTTP_200_OK)
async def delete_album_tag_relation(
    album_cid: int,
    tag_cid: int,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Response:
    relation = AlbumTagBase(album_cid=album_cid, tag_cid=tag_cid)
    await RelationService().delete_album_tag_relation(relation, uow)
    return Response("Ok!")


@router.post("/song-artist/", status_code=status.HTTP_201_CREATED)
async def create_song_artist_relation(
    relation: SongArtistBase,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> SongArtist:
    return await RelationService().create_song_artist_relation(relation, uow)


@router.delete("/song-artist/", status_code=status.HTTP_200_OK)
async def delete_song_artist_relation(
    song_cid: int,
    artist_cid: int,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Response:
    relation = SongArtistBase(song_cid=song_cid, artist_cid=artist_cid)
    await RelationService().delete_song_artist_relation(relation, uow)
    return Response("Ok!")


@router.post("/song-tag/", status_code=status.HTTP_201_CREATED)
async def create_song_tag_relaton(
    relation: SongTagBase,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> SongTag:
    return await RelationService().create_song_tag_relaton(relation, uow)


@router.delete("/song-tag/", status_code=status.HTTP_200_OK)
async def delete_song_tag_relation(
    song_cid: int,
    tag_cid: int,
    uow: UOWDep,
    user: User = Depends(current_super_user),
) -> Response:
    relation = SongTagBase(song_cid=song_cid, tag_cid=tag_cid)
    await RelationService().delete_song_tag_relation(relation, uow)
    return Response("Ok!")
