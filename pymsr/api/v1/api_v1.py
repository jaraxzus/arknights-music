from fastapi import APIRouter

import albums
from pymsr.auth import auth_router

from .routers import albums, artists, offer, relations, search, songs, tags

api_v1 = APIRouter(
    prefix="/api/v1",
)
api_v1.include_router(auth_router.router)
api_v1.include_router(albums.router)
api_v1.include_router(songs.router)
api_v1.include_router(artists.router)
api_v1.include_router(tags.router)
api_v1.include_router(relations.router)
api_v1.include_router(search.router)
api_v1.include_router(offer.router)
