from fastapi import APIRouter

from pymsr.auth.schemas import UserCreate, UserRead, UserUpdate
from pymsr.auth.users import auth_backend, fastapi_users

router = APIRouter(
    prefix="/auth",
    tags=["auth"],
)

router.include_router(
    fastapi_users.get_auth_router(auth_backend),
    prefix="/jwt",
)
router.include_router(
    fastapi_users.get_register_router(UserRead, UserCreate),
)
router.include_router(
    fastapi_users.get_reset_password_router(),
)
router.include_router(
    fastapi_users.get_verify_router(UserRead),
)
router.include_router(
    fastapi_users.get_users_router(UserRead, UserUpdate),
)
