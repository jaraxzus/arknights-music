import uuid

from fastapi import Depends, Request
from fastapi_users import BaseUserManager, FastAPIUsers, UUIDIDMixin, models
from fastapi_users.authentication import (
    AuthenticationBackend,
    CookieTransport,
    JWTStrategy,
)
from fastapi_users.db import SQLAlchemyUserDatabase
from sqlalchemy.ext.asyncio import AsyncSession

from pymsr.common.settings import env_settings, settings
from pymsr.db.db_helper import db_helper
from pymsr.db.sql.models.auth_models import User
from pymsr.services.email import EmailService


class UserManager(UUIDIDMixin, BaseUserManager[User, uuid.UUID]):
    reset_password_token_secret = env_settings.JWT_SECRET
    verification_token_secret = env_settings.JWT_SECRET

    async def on_after_register(self, user: User, request: Request | None = None):
        subject = "Registration on Arkngihts Music"
        content = f"""
Dear {user.email},

We are thrilled to inform you that your registration on Arkngihts Music has been successfully completed. Welcome to our community!

If you did not initiate this registration or are unaware of this process, please disregard this email. Someone might have used your email address by mistake. If you have any concerns or questions, please contact our support team at {settings.env.SMTP_USERNAME}.
            """

        await EmailService().send_mail(to=user.email, subject=subject, content=content)
        # print(f"User {user.email} has registered.")

    async def on_after_forgot_password(
        self, user: User, token: str, request: Request | None = None
    ):
        subject = "Password Reset Request"
        content = f"""
Dear {user.email},

We have received a request to reset the password associated with your account on Arkngihts Music. To proceed with the password reset, please use the following token:

Token: {token}

Please note that this token is valid for a limited time period. If you did not initiate this request or have any concerns, please contact our support team immediately at {settings.env.SMTP_USERNAME}.
        """
        await EmailService().send_mail(user.email, subject, content)
        # print(f"User {user.email} has forgot their password. Reset token: {token}")

    async def on_after_request_verify(
        self, user: User, token: str, request: Request | None = None
    ):
        subject = "Email Verification for Arkngihts Music"
        content = f"""
Hello {user.email},

Welcome to Arkngihts Music! To complete your registration and ensure the security of your account, please verify your email address by using the following token:

Verification Token: {token}

If you haven't registered on Arkngihts Music or did not request this verification, please ignore this email.       
        """
        await EmailService().send_mail(user.email, subject, content)
        # print(f"Verification requested for user {user.id}. Verification token: {token}")


async def get_user_db(
    session: AsyncSession = Depends(db_helper.get_session),
):
    yield SQLAlchemyUserDatabase(session, User)


async def get_user_manager(
    user_db: SQLAlchemyUserDatabase = Depends(get_user_db),
):
    yield UserManager(user_db)


cookie_transport = CookieTransport(cookie_name="pymsr", cookie_max_age=36000)


def get_jwt_strategy() -> JWTStrategy:
    return JWTStrategy(secret=env_settings.JWT_SECRET, lifetime_seconds=36000)


auth_backend = AuthenticationBackend(
    name="jwt",
    transport=cookie_transport,
    get_strategy=get_jwt_strategy,
)

fastapi_users = FastAPIUsers[User, uuid.UUID](get_user_manager, [auth_backend])

current_active_user = fastapi_users.current_user(active=True)
current_super_user = fastapi_users.current_user(superuser=True)
current_verified_user = fastapi_users.current_user(verified=True)
