from typing import TYPE_CHECKING

from pydantic import BaseModel, alias_generators

if TYPE_CHECKING:
    from .artist import Artist
    from .song import SongCamelCaseInput, SongDetailWithArtistsAndTags
    from .tag import Tag


class AlbumBase(BaseModel):
    """with name"""

    name: str


class AlbumInput(
    AlbumBase,
):
    """with cid, name, cover_url and artistes list[str]"""

    cid: int
    cover_url: str
    artistes: list[str]

    @property
    def url(self) -> str:
        return f"https://monster-siren.hypergryph.com/api/album/{self.cid}/detail"

    def __str__(self) -> str:
        return f"Album: {self.name}"


class AlbumCamelCaseInput(AlbumInput):
    class Config:
        alias_generator = alias_generators.to_camel


class AlbumDetailBaseInput(BaseModel):
    name: str
    cover_url: str
    intro: str
    belong: str
    cover_de_url: str


class AlbumDetailBasePatchInput(AlbumBase):
    name: str | None = None
    cover_url: str | None = None
    intro: str | None = None
    belong: str | None = None
    cover_de_url: str | None = None


class AlbumDetailBase(AlbumDetailBaseInput):
    cid: int


class AlbumDetail(AlbumDetailBase):
    songs: list["SongCamelCaseInput"]

    def __str__(self) -> str:
        return f"""
Album title: {self.name}
Album intro: {self.intro}
Album belong: {self.belong}
Songs: {self.songs}
        """


class AlbumDetailCamelCaseInput(AlbumDetail):
    class Config:
        alias_generator = alias_generators.to_camel


class TagsAndArtists(BaseModel):
    tags: list["Tag"]
    artists: list["Artist"]


class AlbumDetailWithArtistAndTags(AlbumDetailBase, TagsAndArtists):
    pass


class AlbumDetailFull(AlbumDetail, TagsAndArtists):
    songs: list["SongDetailWithArtistsAndTags"]
