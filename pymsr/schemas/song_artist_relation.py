from pydantic import BaseModel


class SongArtistBase(BaseModel):
    song_cid: int
    artist_cid: int


class SongArtist(SongArtistBase):
    cid: int
