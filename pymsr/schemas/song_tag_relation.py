from pydantic import BaseModel


class SongTagBase(BaseModel):
    song_cid: int
    tag_cid: int


class SongTag(SongTagBase):
    cid: int
