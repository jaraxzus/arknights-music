from pydantic import BaseModel


class AlbumTagBase(BaseModel):
    album_cid: int
    tag_cid: int


class AlbumTag(AlbumTagBase):
    cid: int
