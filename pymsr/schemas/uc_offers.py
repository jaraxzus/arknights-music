from pydantic import BaseModel

from pymsr.auth.schemas import UserRead
from pymsr.common.enums import Operation
from pymsr.schemas.song import SongDetail

from .album import AlbumDetailBase


class OnDB(BaseModel):
    cid: int
    user: UserRead | None = None


class AlbumArtistOfferBase(BaseModel):
    album_cid: int
    artist_cid: int | None = None
    message: str
    artist: str
    operation: Operation

    class Config:
        use_enum_values = True


class AlbumArtistOffer(AlbumArtistOfferBase, OnDB):
    album: AlbumDetailBase | None = None


class AlbumTagOfferBase(BaseModel):
    album_cid: int
    tag_cid: int | None = None
    message: str
    tag: str
    operation: Operation

    class Config:
        use_enum_values = True


class AlbumTagOffer(AlbumTagOfferBase, OnDB):
    album: AlbumDetailBase | None = None


class SongArtistOfferBase(BaseModel):
    song_cid: int
    artist_cid: int | None = None
    message: str
    artist: str
    operation: Operation

    class Config:
        use_enum_values = True


class SongArtistOffer(SongArtistOfferBase, OnDB):
    song: SongDetail | None = None


class SongTagOfferBase(BaseModel):
    song_cid: int
    tag_cid: int | None = None
    message: str
    tag: str
    operation: Operation

    class Config:
        use_enum_values = True


class SongTagOffer(SongTagOfferBase, OnDB):
    song: SongDetail | None = None
