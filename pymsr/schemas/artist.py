from typing import TYPE_CHECKING

from pydantic import BaseModel

if TYPE_CHECKING:
    from .album import AlbumDetailFull
    from .song import SongDetail


class BaseArtist(BaseModel):
    name: str


class Artist(BaseArtist):
    cid: int

    def __str__(self) -> str:
        return f"cid: {self.cid}, name: {self.name}"


# class ArtistWithSongs(Artist):
#     songs: list["SongDetail"]


class ArtistWithAlbums(Artist):
    albums: list["AlbumDetailFull"]


# class ArtistFull(ArtistWithAlbums, ArtistWithSongs):
#     pass
