from pydantic import BaseModel


class TagBase(BaseModel):
    tag: str


class Tag(TagBase):
    cid: int
