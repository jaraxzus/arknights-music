from typing import TYPE_CHECKING

from pydantic import BaseModel, alias_generators

if TYPE_CHECKING:
    from .artist import Artist
    from .tag import Tag


class SongCamelCaseInput(BaseModel):
    """song configured by camel case input validation
    Params
        cid: int,
        name: str,
        artistes: list[str]"""

    cid: int
    name: str
    artistes: list[str]

    class Config:
        alias_generator = alias_generators.to_camel

    @property
    def url(self) -> str:
        return f"https://monster-siren.hypergryph.com/api/song/{self.cid}"

    def __eq__(self, other):
        if isinstance(other, "SongDetailWithArtistsAndTags"):
            return self.cid == other.cid
        return False

    def __str__(self) -> str:
        return f"{self.name}"


class SongDetailBaseForPatch(BaseModel):
    """Scheme for partial update"""

    name: str | None
    source_url: str | None
    lyric_url: str | None
    mv_url: str | None
    mv_cover_url: str | None


class SongDetailBase(BaseModel):
    """Base songs scheme with
    name: str
    album_cid: int
    source_url: str
    lyric_url: str | None
    mv_url: str | None
    mv_cover_url: str | None
    """

    name: str
    album_cid: int
    source_url: str
    lyric_url: str | None
    mv_url: str | None
    mv_cover_url: str | None

    @property
    def file_name(self) -> str:
        """return file name based on song name + defoult song extinsion"""
        return f"{self.name}.{self.source_url.split('.')[-1]}"

    def __str__(self) -> str:
        return f"""
Name: {self.name}
Url: {self.source_url}"""


class SongDetail(SongDetailBase):
    """class on msr based or data-based model with cid"""

    cid: int

    # def __eq__(self, other):
    #     if isinstance(other, SongCamelCaseInput):
    #         return self.cid == other.cid
    #     return False


class SongDetailCamelCaseInput(SongDetail):
    """Camel case input model with artists: list[str]"""

    artists: list[str]

    class Config:
        alias_generator = alias_generators.to_camel


class SongDetailWithArtistsAndTags(SongDetail):
    """Song with artists and tags"""

    artists: list["Artist"]
    tags: list["Tag"]
