__all__ = (
    "Artist",
    "BaseArtist",
    "ArtistWithAlbums",
    "SongCamelCaseInput",
    "SongDetail",
    "SongDetailBase",
    "SongDetailBaseForPatch",
    "SongDetailCamelCaseInput",
    "SongDetailWithArtistsAndTags",
    "Tag",
    "TagBase",
    "AlbumBase",
    "AlbumCamelCaseInput",
    "AlbumDetail",
    "AlbumDetailBase",
    "AlbumDetailBaseInput",
    "AlbumDetailCamelCaseInput",
    "AlbumDetailWithArtistAndTags",
    "AlbumDetailFull",
    "AlbumInput",
    "AlbumArtist",
    "AlbumArtistBase",
    "SongArtist",
    "SongArtistBase",
    "AlbumTag",
    "AlbumTagBase",
    "SongTag",
    "SongTagBase",
    "AlbumArtistOffer",
    "AlbumArtistOfferBase",
    "AlbumTagOffer",
    "AlbumTagOfferBase",
    "SongArtistOffer",
    "SongArtistOfferBase",
    "SongTagOffer",
    "SongTagOfferBase",
)
from .album import (
    AlbumBase,
    AlbumCamelCaseInput,
    AlbumDetail,
    AlbumDetailBase,
    AlbumDetailBaseInput,
    AlbumDetailCamelCaseInput,
    AlbumDetailFull,
    AlbumDetailWithArtistAndTags,
    AlbumInput,
)
from .album_artist_relation import AlbumArtist, AlbumArtistBase
from .album_tag_relation import AlbumTag, AlbumTagBase
from .artist import Artist, ArtistWithAlbums, BaseArtist
from .song import (
    SongCamelCaseInput,
    SongDetail,
    SongDetailBase,
    SongDetailBaseForPatch,
    SongDetailCamelCaseInput,
    SongDetailWithArtistsAndTags,
)
from .song_artist_relation import SongArtist, SongArtistBase
from .song_tag_relation import SongTag, SongTagBase
from .tag import Tag, TagBase
from .uc_offers import (
    AlbumArtistOffer,
    AlbumArtistOfferBase,
    AlbumTagOffer,
    AlbumTagOfferBase,
    SongArtistOffer,
    SongArtistOfferBase,
    SongTagOffer,
    SongTagOfferBase,
)

Artist.model_rebuild()
BaseArtist.model_rebuild()
ArtistWithAlbums.model_rebuild()
SongCamelCaseInput.model_rebuild()
SongDetail.model_rebuild()
SongDetailBase.model_rebuild()
SongDetailBaseForPatch.model_rebuild()
SongDetailCamelCaseInput.model_rebuild()
SongDetailWithArtistsAndTags.model_rebuild()
Tag.model_rebuild()
TagBase.model_rebuild()
AlbumBase.model_rebuild()
AlbumCamelCaseInput.model_rebuild()
AlbumDetail.model_rebuild()
AlbumDetailBase.model_rebuild()
AlbumDetailBaseInput.model_rebuild()
AlbumDetailCamelCaseInput.model_rebuild()
AlbumDetailFull.model_rebuild()
AlbumInput.model_rebuild()
AlbumDetailWithArtistAndTags.model_rebuild()
