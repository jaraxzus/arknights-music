from pydantic import BaseModel


class AlbumArtistBase(BaseModel):
    album_cid: int
    artist_cid: int


class AlbumArtist(AlbumArtistBase):
    cid: int
