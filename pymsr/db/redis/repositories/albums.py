from .redis_repository import RedisRepository


class RedisAlbumRepository(RedisRepository):
    key_prefix = "album_"
