import redis.asyncio as redis

from pymsr.db.abc_repository import AbstractRepository

from ...abc_repository import AbstractRepository
from ..redis_helper import redis_helper


class RedisRepository(AbstractRepository):
    key_prefix: str | None

    async def add(self, data: dict):
        if self.key_prefix is None:
            raise ValueError("key prefix is not set.")
        async with redis_helper.session() as session:
            await session.set(f"{self.key_prefix}{data['key']}", data["value"])

    async def get(self, cid):
        if self.key_prefix is None:
            raise ValueError("key prefix is not set.")
        async with redis_helper.session() as session:
            return await session.get(f"{self.key_prefix}{cid}")

    async def partial_update(self, data: dict):
        if self.key_prefix is None:
            raise ValueError("key prefix is not set.")
        return await self.add(data)

    async def delete(self, cid):
        if self.key_prefix is None:
            raise ValueError("key prefix is not set.")
        async with redis_helper.session() as session:
            await session.delete(f"{self.key_prefix}{cid}")
