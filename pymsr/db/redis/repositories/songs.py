from .redis_repository import RedisRepository


class RedisSongsRepository(RedisRepository):
    key_prefix = "song_"
