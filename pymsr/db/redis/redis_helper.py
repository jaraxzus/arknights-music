#
import contextlib
from typing import AsyncIterator

import redis.asyncio as aioredis

from pymsr.common.settings import settings


class RedisHelper:
    def __init__(
        self,
        url: str,
    ) -> None:
        self.pool = aioredis.ConnectionPool.from_url(url)

    @contextlib.asynccontextmanager
    async def session(self) -> AsyncIterator[aioredis.Redis]:
        async with aioredis.Redis(connection_pool=self.pool) as session:
            try:
                yield session
            except Exception as ex:
                print(ex)
                raise


redis_helper = RedisHelper(settings.env.REDIS_URL)
