from sqlalchemy import select

from pymsr.db.sql.models import Tag

from ...abc_repository import AbstractTagArtistReposityry
from .sql_alchemy_repository import SQLAlchemyRepository


class TagsRepository(SQLAlchemyRepository, AbstractTagArtistReposityry):
    model = Tag

    async def get_list(self) -> list[Tag]:
        res = await self.session.execute(select(Tag))
        return list(res.scalars())

    async def get_by_tag(self, tag: str) -> Tag | None:
        res = await self.session.scalar(select(Tag).where(Tag.tag == tag))
        return res
