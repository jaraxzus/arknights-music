from sqlalchemy import and_, select

from ..models import AlbumArtistAsociation
from ..repositories.sql_alchemy_repository import SQLAlchemyMTORepository


class AlbumArtistRepository(SQLAlchemyMTORepository):
    model = AlbumArtistAsociation

    async def delete(self, relation: dict) -> None:
        if self.model is None:
            raise ValueError("Model is not set.")

        db_relation = await self.session.execute(
            select(self.model).where(
                and_(
                    AlbumArtistAsociation.album_cid == relation["album_cid"],
                    AlbumArtistAsociation.artist_cid == relation["artist_cid"],
                )
            )
        )
        db_relation = db_relation.scalar_one()
        await self.session.delete(db_relation)
