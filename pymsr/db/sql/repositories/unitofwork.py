from abc import ABC, abstractmethod

from sqlalchemy.ext.asyncio import AsyncSession

from pymsr.db.db_helper import db_helper

from .album_artist import AlbumArtistRepository
from .album_tag import AlbumTagRepository
from .albums import AlbumsRepository
from .artists import ArtistsRepository
from .offers_repository import (OfferAlbumArtistReposytory,
                                OfferAlbumTagReposytory,
                                OfferSongArtistReposytory,
                                OfferSongTagReposytory)
from .song_artist import SongArtistRepository
from .song_tag import SongTagRepository
from .songs import SongsRepository
from .tags import TagsRepository


class IUnitOfWork(ABC):
    session: AsyncSession
    albums: AlbumsRepository
    artists: ArtistsRepository
    songs: SongsRepository
    tags: TagsRepository
    album_artist: AlbumArtistRepository
    album_tag: AlbumTagRepository
    song_artist: SongArtistRepository
    song_tag: SongTagRepository
    offer_album_artist: OfferAlbumArtistReposytory
    offer_album_tag: OfferAlbumTagReposytory
    offer_song_artist: OfferSongArtistReposytory
    offer_song_tag: OfferSongTagReposytory

    @abstractmethod
    def __init__(self):
        ...

    @abstractmethod
    async def __aenter__(self):
        ...

    @abstractmethod
    async def __aexit__(self, *args):
        ...

    @abstractmethod
    async def commit(self):
        ...

    @abstractmethod
    async def rollback(self):
        ...


class UnitOfWork(IUnitOfWork):
    def __init__(self):
        self.session_factory = db_helper.session_factory

    async def __aenter__(self):
        self.session = self.session_factory()

        self.albums = AlbumsRepository(self.session)
        self.artists = ArtistsRepository(self.session)
        self.songs = SongsRepository(self.session)
        self.tags = TagsRepository(self.session)

        self.album_artist = AlbumArtistRepository(self.session)
        self.album_tag = AlbumTagRepository(self.session)
        self.song_artist = SongArtistRepository(self.session)
        self.song_tag = SongTagRepository(self.session)

        self.offer_album_artist = OfferAlbumArtistReposytory(self.session)
        self.offer_album_tag = OfferAlbumTagReposytory(self.session)
        self.offer_song_artist = OfferSongArtistReposytory(self.session)
        self.offer_song_tag = OfferSongTagReposytory(self.session)
        self.uc_song_tag = OfferAlbumTagReposytory(self.session)

    async def __aexit__(self, *args):
        await self.rollback()
        await self.session.close()

    async def commit(self):
        await self.session.commit()

    async def rollback(self):
        await self.session.rollback()
