from sqlalchemy import or_, select
from sqlalchemy.orm import aliased, contains_eager, selectinload

from ...abc_repository import AbstractAlbumRepository
from ..models import Album, Song
from ..models.album_artist_association import AlbumArtistAsociation
from ..models.artist import Artist
from ..models.song_artist_association import SongArtistAsociation
from ..models.tag import Tag
from ..models.tag_album_association import TagAlbumAsociation
from ..models.tag_song_association import TagSongAsociation
from .sql_alchemy_repository import SQLAlchemyRepository


class AlbumsRepository(SQLAlchemyRepository, AbstractAlbumRepository):
    model = Album

    async def get_full_album(self, cid: int) -> Album:
        """Возвращает полный обьект альбома с всеми вложеностями песен и артисов"""
        query = (
            select(Album)
            .options(
                selectinload(Album.songs).options(
                    selectinload(Song.artists), selectinload(Song.tags)
                )
            )
            .options(selectinload(Album.artists))
            .options(selectinload(Album.tags))
            .where(Album.cid == cid)
        )
        res = await self.session.execute(query)
        return res.scalar_one()

    async def get_list(self) -> list[Album]:
        res = await self.session.execute(
            select(Album).options(selectinload(Album.artists), selectinload(Album.tags))
        )
        return list(res.scalars().all())

    async def search(self, value: str) -> list[Album]:
        # SELECT *
        # FROM albums a
        # -- джоиним артистов
        # JOIN album_artist_association aaa ON aaa.album_cid =a.cid
        # JOIN artists a2 ON aaa.artist_cid = a2.cid
        # -- джоиним теги
        # LEFT JOIN tag_album_association taa ON taa.album_cid = a.cid
        # LEFT JOIN tags t ON t.cid = taa.tag_cid
        # -- джоними песни
        # JOIN songs s ON s.album_cid = a.cid
        # -- джоиним артистов к песням
        # JOIN song_artist_association saa ON saa.song_cid = s.cid
        # JOIN artists a3 ON a3.cid = saa.artist_cid
        # -- джоиним теги к песням
        # LEFT JOIN tag_song_association tsa ON tsa.song_cid = s.cid
        # LEFT JOIN tags t2 ON tsa.tag_cid = t2.cid
        # WHERE a.name LIKE '%生存之形%'
        # 	OR a2.name LIKE	'%生存之形%'
        # 	OR t.tag LIKE '%生存之形%'
        # 	OR s.name LIKE '%生存之形%'
        # 	OR a3.name LIKE '%生存之形%'
        # 	OR t2.tag LIKE '%生存之形%'
        aliased_artist = aliased(Artist, name="artist_1")
        aliased_tag = aliased(Tag, name="tag_2")

        subq = (
            select(Song.cid)
            .select_from(Album)
            .outerjoin(
                AlbumArtistAsociation, AlbumArtistAsociation.album_cid == Album.cid
            )
            .outerjoin(Artist, AlbumArtistAsociation.artist_cid == Artist.cid)
            .outerjoin(TagAlbumAsociation, Album.cid == TagAlbumAsociation.album_cid)
            .outerjoin(Tag, TagAlbumAsociation.tag_cid == Tag.cid)
            .outerjoin(Song, Album.cid == Song.album_cid)
            .outerjoin(SongArtistAsociation, SongArtistAsociation.song_cid == Song.cid)
            .outerjoin(
                aliased_artist, SongArtistAsociation.artist_cid == aliased_artist.cid
            )
            .outerjoin(TagSongAsociation, TagSongAsociation.song_cid == Song.cid)
            .outerjoin(aliased_tag, aliased_tag.cid == TagSongAsociation.tag_cid)
            .where(
                or_(
                    Album.name.ilike(f"%{value}%"),
                    Artist.name.ilike(f"%{value}%"),
                    Tag.tag.ilike(f"%{value}%"),
                    Song.name.ilike(f"%{value}%"),
                    aliased_artist.name.ilike(f"%{value}%"),
                    aliased_tag.tag.ilike(f"%{value}%"),
                )
            )
            .distinct()
            .scalar_subquery()
            .correlate(Song)
        )
        query = (
            select(Album, Artist, Tag, Song, Artist, Tag)
            .outerjoin(
                AlbumArtistAsociation, AlbumArtistAsociation.album_cid == Album.cid
            )
            .outerjoin(Artist, AlbumArtistAsociation.artist_cid == Artist.cid)
            .outerjoin(TagAlbumAsociation, Album.cid == TagAlbumAsociation.album_cid)
            .outerjoin(Tag, TagAlbumAsociation.tag_cid == Tag.cid)
            .join(Song, Album.cid == Song.album_cid)
            .options(
                contains_eager(Album.artists),
                contains_eager(Album.tags),
                contains_eager(Album.songs).options(
                    selectinload(Song.tags),
                    selectinload(Song.artists),
                ),
            )
            .where(Song.cid.in_(subq))
        )
        # print(query.compile(compile_kwargs={"literal_binds": True}))
        result = await self.session.execute(query)
        return list(result.scalars().unique().all())

    async def search_test(self, value: str) -> list[int]:
        aliased_artist = aliased(Artist, name="artist_1")
        aliased_tag = aliased(Tag, name="tag_2")

        query = (
            select(Song.cid)
            .select_from(Album)
            .outerjoin(
                AlbumArtistAsociation, AlbumArtistAsociation.album_cid == Album.cid
            )
            .outerjoin(Artist, AlbumArtistAsociation.artist_cid == Artist.cid)
            .outerjoin(TagAlbumAsociation, Album.cid == TagAlbumAsociation.album_cid)
            .outerjoin(Tag, TagAlbumAsociation.tag_cid == Tag.cid)
            .outerjoin(Song, Album.cid == Song.album_cid)
            .outerjoin(SongArtistAsociation, SongArtistAsociation.song_cid == Song.cid)
            .outerjoin(
                aliased_artist, SongArtistAsociation.artist_cid == aliased_artist.cid
            )
            .outerjoin(TagSongAsociation, TagSongAsociation.song_cid == Song.cid)
            .outerjoin(aliased_tag, aliased_tag.cid == TagSongAsociation.tag_cid)
            .where(
                or_(
                    Album.name.ilike(f"%{value}%"),
                    Artist.name.ilike(f"%{value}%"),
                    Tag.tag.ilike(f"%{value}%"),
                    Song.name.ilike(f"%{value}%"),
                    aliased_artist.name.ilike(f"%{value}%"),
                    aliased_tag.tag.ilike(f"%{value}%"),
                )
            )
            .distinct()
            .correlate(Song)
        )

        # print(query.compile(compile_kwargs={"literal_binds": True}))
        result = await self.session.execute(query)
        return list(result.scalars().unique().all())
