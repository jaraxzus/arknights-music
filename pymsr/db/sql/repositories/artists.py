from sqlalchemy import select

from ...abc_repository import AbstractTagArtistReposityry
from ..models import Artist
from .sql_alchemy_repository import SQLAlchemyRepository


class ArtistsRepository(SQLAlchemyRepository, AbstractTagArtistReposityry):
    model = Artist

    async def get_list(self) -> list[Artist]:
        res = await self.session.execute(select(Artist))
        return list(res.scalars())

    async def get_by_name(self, name: str) -> Artist | None:
        res = await self.session.scalar(select(Artist).where(Artist.name == name))
        return res
