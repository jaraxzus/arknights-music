from sqlalchemy import and_, select

from ..models import SongArtistAsociation
from .sql_alchemy_repository import SQLAlchemyMTORepository


class SongArtistRepository(SQLAlchemyMTORepository):
    model = SongArtistAsociation

    async def delete(self, relation: dict) -> None:
        if self.model is None:
            raise ValueError("Model is not set.")

        db_relation = await self.session.execute(
            select(self.model).where(
                and_(
                    SongArtistAsociation.song_cid == relation["song_cid"],
                    SongArtistAsociation.artist_cid == relation["artist_cid"],
                )
            )
        )
        db_relation = db_relation.scalar_one()
        await self.session.delete(db_relation)
