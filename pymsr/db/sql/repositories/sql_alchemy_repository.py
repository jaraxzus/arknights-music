from sqlalchemy import select
from sqlalchemy.dialects.postgresql import insert
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import contains_eager

from pymsr.db.sql.models import User
from pymsr.db.sql.models.album import Album
from pymsr.db.sql.models.song import Song

from ...abc_repository import AbstracOfferRepository, AbstractRepository
from ..models import AbstractOffer, CidBase


class SQLAlchemyMTORepository:
    model: type[CidBase] | None = None

    def __init__(self, session: AsyncSession) -> None:
        self.session = session

    async def add(self, data: dict):
        if self.model is None:
            raise ValueError("Model is not set.")
        # print(f"{self.model.__name__}\n{data}")
        stmt = insert(self.model).values(**data).returning(self.model)
        res = await self.session.execute(stmt)
        return res.scalar_one()


class SQLAlchemyOfferRepository(AbstracOfferRepository):
    model: type[AbstractOffer] | None = None

    def __init__(self, session: AsyncSession) -> None:
        self.session = session

    async def get_list(self):
        pass

    async def get(self, cid: int):
        if self.model is None:
            raise ValueError("Model is not set.")
        query = select(self.model).where(self.model.cid == cid)
        res = await self.session.execute(query)
        return res.scalar_one()

    async def add(self, data: dict):
        if self.model is None:
            raise ValueError("Model is not set.")
        # print(f"{self.model.__name__}\n{data}")
        stmt = insert(self.model).values(**data).returning(self.model)
        res = await self.session.execute(stmt)
        return res.scalar_one()

    async def delete(self, cid: int) -> int:
        if self.model is None:
            raise ValueError("Model is not set.")
        album = await self.session.get_one(self.model, cid)
        await self.session.delete(album)
        return album.cid


class SQLAlchemyRepository(AbstractRepository):
    model: type[CidBase] | None = None

    def __init__(self, session: AsyncSession):
        self.session = session

    async def add(self, data: dict):
        if self.model is None:
            raise ValueError("Model is not set.")
        stmt = insert(self.model).values(**data).returning(self.model)
        res = await self.session.execute(stmt)
        return res.scalar_one()

    async def add_all(self, values: list[dict]) -> list:
        if self.model is None:
            raise ValueError("Model is not set.")
        results = []
        for value in values:
            result = await self.session.execute(
                insert(self.model)
                .values(**value)
                .on_conflict_do_update(index_elements=["cid"], set_=value)
                .returning(self.model)
            )
            results.append(result.scalar_one())

        await self.session.flush()
        return results

    async def get(self, cid: int):
        if self.model is None:
            raise ValueError("Model is not set.")
        query = select(self.model).where(self.model.cid == cid)
        res = await self.session.execute(query)
        return res.scalar_one()

    async def partial_update(self, data: dict):
        if self.model is None:
            raise ValueError("Model is not set.")
        stored_data = await self.session.get_one(self.model, data["cid"])
        for field, value in data.items():
            setattr(stored_data, field, value)
        await self.session.flush()
        return stored_data

    async def delete(self, cid: int) -> int:
        if self.model is None:
            raise ValueError("Model is not set.")
        album = await self.session.get_one(self.model, cid)
        await self.session.delete(album)
        return album.cid
