from sqlalchemy import select
from sqlalchemy.orm import contains_eager

from pymsr.db.sql.models.album import Album
from pymsr.db.sql.models.auth_models import User
from pymsr.db.sql.models.song import Song

from ..models import (AlbumArtistOffer, AlbumTagOffer, SongArtistOffer,
                      SongTagOffer)
from .sql_alchemy_repository import SQLAlchemyOfferRepository


class OfferSongArtistReposytory(SQLAlchemyOfferRepository):
    model = SongArtistOffer

    async def get_list(self) -> list[SongArtistOffer]:
        if self.model is None:
            raise ValueError("Model is not set.")
        res = await self.session.execute(
            select(SongArtistOffer)
            .join(User, SongArtistOffer.user_id == User.id)
            .join(Song, SongArtistOffer.song_cid == Song.cid)
            .options(contains_eager(SongArtistOffer.user))
            .options(contains_eager(SongArtistOffer.song))
        )
        return list(res.scalars())


class OfferSongTagReposytory(SQLAlchemyOfferRepository):
    model = SongTagOffer

    async def get_list(self) -> list[SongTagOffer]:
        if self.model is None:
            raise ValueError("Model is not set.")
        res = await self.session.execute(
            select(SongTagOffer)
            .join(User, SongTagOffer.user_id == User.id)
            .join(Song, SongTagOffer.song_cid == Song.cid)
            .options(contains_eager(SongTagOffer.user))
            .options(contains_eager(SongTagOffer.song))
        )
        return list(res.scalars())


class OfferAlbumArtistReposytory(SQLAlchemyOfferRepository):
    model = AlbumArtistOffer

    async def get_list(self) -> list[AlbumArtistOffer]:
        if self.model is None:
            raise ValueError("Model is not set.")
        res = await self.session.execute(
            select(AlbumArtistOffer)
            .join(User, AlbumArtistOffer.user_id == User.id)
            .join(Album, AlbumArtistOffer.album_cid == Album.cid)
            .options(contains_eager(AlbumArtistOffer.user))
            .options(contains_eager(AlbumArtistOffer.album))
        )
        return list(res.scalars())


class OfferAlbumTagReposytory(SQLAlchemyOfferRepository):
    model = AlbumTagOffer

    async def get_list(self) -> list[AlbumTagOffer]:
        if self.model is None:
            raise ValueError("Model is not set.")
        res = await self.session.execute(
            select(AlbumTagOffer)
            .join(User, AlbumTagOffer.user_id == User.id)
            .join(Album, AlbumTagOffer.album_cid == Album.cid)
            .options(contains_eager(AlbumTagOffer.user))
            .options(contains_eager(AlbumTagOffer.album))
        )
        return list(res.scalars())
