from sqlalchemy import and_, select

from ..models import TagSongAsociation
from .sql_alchemy_repository import SQLAlchemyMTORepository


class SongTagRepository(SQLAlchemyMTORepository):
    model = TagSongAsociation

    async def delete(self, relation: dict) -> None:
        if self.model is None:
            raise ValueError("Model is not set.")

        db_relation = await self.session.execute(
            select(self.model).where(
                and_(
                    TagSongAsociation.song_cid == relation["song_cid"],
                    TagSongAsociation.tag_cid == relation["tag_cid"],
                )
            )
        )
        db_relation = db_relation.scalar_one()
        await self.session.delete(db_relation)
