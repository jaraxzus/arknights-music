from sqlalchemy import select
from sqlalchemy.orm import selectinload

from ...abc_repository import AbstractSongRepository
from ..models import Song
from .sql_alchemy_repository import SQLAlchemyRepository


class SongsRepository(SQLAlchemyRepository, AbstractSongRepository):
    model = Song

    async def get(self, cid: int) -> Song:
        res = await self.session.execute(
            select(Song)
            .options(selectinload(Song.artists), selectinload(Song.tags))
            .where(Song.cid == cid)
        )
        return res.scalar_one()

    async def get_all(self) -> list[Song]:
        res = await self.session.execute(
            select(Song).options(selectinload(Song.artists), selectinload(Song.tags))
        )
        return list(res.scalars().all())
