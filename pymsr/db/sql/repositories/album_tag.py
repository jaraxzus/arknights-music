from sqlalchemy import and_, select

from ..models import TagAlbumAsociation
from .sql_alchemy_repository import SQLAlchemyMTORepository


class AlbumTagRepository(SQLAlchemyMTORepository):
    model = TagAlbumAsociation

    async def delete(self, relation: dict) -> None:
        if self.model is None:
            raise ValueError("Model is not set.")

        db_relation = await self.session.execute(
            select(self.model).where(
                and_(
                    TagAlbumAsociation.album_cid == relation["album_cid"],
                    TagAlbumAsociation.tag_cid == relation["tag_cid"],
                )
            )
        )
        db_relation = db_relation.scalar_one()
        await self.session.delete(db_relation)
