from typing import TYPE_CHECKING

from sqlalchemy.orm import Mapped, mapped_column, relationship

if TYPE_CHECKING:
    from .album import Album
    from .song import Song
    from .tag import Tag

from .base import CidBase


class Artist(CidBase):
    cid: Mapped[int] = mapped_column(autoincrement=True, primary_key=True)
    name: Mapped[str] = mapped_column(unique=True, index=True)

    albums: Mapped[list["Album"]] = relationship(
        secondary="album_artist_association", back_populates="artists"
    )
    songs: Mapped[list["Song"]] = relationship(
        secondary="song_artist_association", back_populates="artists"
    )
    tags: Mapped[list["Tag"]] = relationship(
        secondary="tag_artist_association", back_populates="artists"
    )
