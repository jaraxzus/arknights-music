from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column

from .base import CidBase


class SongArtistAsociation(CidBase):
    __tablename__ = "song_artist_association"
    __table_args__ = (
        UniqueConstraint("song_cid", "artist_cid", name="idx_unique_song_artist"),
    )
    song_cid: Mapped[int] = mapped_column(ForeignKey("songs.cid", ondelete="CASCADE"))
    artist_cid: Mapped[int] = mapped_column(
        ForeignKey("artists.cid", ondelete="CASCADE")
    )
