import uuid
from typing import TYPE_CHECKING

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

if TYPE_CHECKING:
    from .auth_models import User
    from .album import Album
    from .song import Song

from .base import Base, CidBase


class AbstractOffer(CidBase):
    __abstract__ = True

    user_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("user.id", ondelete="CASCADE")
    )
    message: Mapped[str]
    operation: Mapped[str]
    user: Mapped["User"]


class AlbumArtistOffer(AbstractOffer):
    album_cid: Mapped[int] = mapped_column(ForeignKey("albums.cid", ondelete="CASCADE"))
    artist_cid: Mapped[int | None] = mapped_column(
        ForeignKey("artists.cid", ondelete="CASCADE")
    )
    artist: Mapped[str]
    user: Mapped["User"] = relationship(back_populates="album_artist_offers")
    album: Mapped["Album"] = relationship(back_populates="artist_offers")


class AlbumTagOffer(AbstractOffer):
    album_cid: Mapped[int] = mapped_column(ForeignKey("albums.cid", ondelete="CASCADE"))
    tag_cid: Mapped[int | None] = mapped_column(
        ForeignKey("tags.cid", ondelete="CASCADE")
    )
    tag: Mapped[str]
    user: Mapped["User"] = relationship(back_populates="album_tag_offers")
    album: Mapped["Album"] = relationship(back_populates="tag_offers")


class SongArtistOffer(AbstractOffer):
    song_cid: Mapped[int] = mapped_column(ForeignKey("songs.cid", ondelete="CASCADE"))
    artist_cid: Mapped[int | None] = mapped_column(
        ForeignKey("artists.cid", ondelete="CASCADE")
    )
    artist: Mapped[str]
    user: Mapped["User"] = relationship(back_populates="song_artist_offers")
    song: Mapped["Song"] = relationship(back_populates="artist_offers")


class SongTagOffer(AbstractOffer):
    song_cid: Mapped[int] = mapped_column(ForeignKey("songs.cid", ondelete="CASCADE"))
    tag_cid: Mapped[int | None] = mapped_column(
        ForeignKey("tags.cid", ondelete="CASCADE")
    )
    tag: Mapped[str]
    user: Mapped["User"] = relationship(back_populates="song_tag_offers")
    song: Mapped["Song"] = relationship(back_populates="tag_offers")
