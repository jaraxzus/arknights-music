from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column

from .base import CidBase


class AlbumArtistAsociation(CidBase):
    __tablename__ = "album_artist_association"
    __table_args__ = (
        UniqueConstraint("album_cid", "artist_cid", name="idx_unique_album_artist"),
    )
    album_cid: Mapped[int] = mapped_column(ForeignKey("albums.cid", ondelete="CASCADE"))
    artist_cid: Mapped[int] = mapped_column(
        ForeignKey("artists.cid", ondelete="CASCADE")
    )
