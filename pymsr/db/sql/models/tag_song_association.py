from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column

from .base import CidBase


class TagSongAsociation(CidBase):
    __tablename__ = "tag_song_association"
    __table_args__ = (
        UniqueConstraint("song_cid", "tag_cid", name="idx_unique_tag_song"),
    )
    song_cid: Mapped[int] = mapped_column(ForeignKey("songs.cid", ondelete="CASCADE"))
    tag_cid: Mapped[int] = mapped_column(ForeignKey("tags.cid", ondelete="CASCADE"))
