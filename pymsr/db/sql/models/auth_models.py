import uuid
from typing import TYPE_CHECKING

from fastapi_users.db import SQLAlchemyBaseUserTableUUID
from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column, relationship

if TYPE_CHECKING:
    from .offer_models import (
        AlbumArtistOffer,
        AlbumTagOffer,
        SongArtistOffer,
        SongTagOffer,
    )

from .base import Base, CidBase


class UserRoleAsociation(CidBase):
    __tablename__ = "user_role_association"
    __table_args__ = (
        UniqueConstraint("user_id", "role_cid", name="idx_unique_user_role"),
    )
    user_id: Mapped[uuid.UUID] = mapped_column(
        ForeignKey("user.id", ondelete="CASCADE")
    )
    role_cid: Mapped[int] = mapped_column(ForeignKey("roles.cid", ondelete="CASCADE"))


class User(SQLAlchemyBaseUserTableUUID, Base):
    roles: Mapped[list["Role"]] = relationship(
        secondary="user_role_association", back_populates="users"
    )

    album_artist_offers: Mapped[list["AlbumArtistOffer"]] = relationship(
        back_populates="user", cascade="delete"
    )
    album_tag_offers: Mapped[list["AlbumTagOffer"]] = relationship(
        back_populates="user", cascade="delete"
    )
    song_artist_offers: Mapped[list["SongArtistOffer"]] = relationship(
        back_populates="user", cascade="delete"
    )
    song_tag_offers: Mapped[list["SongTagOffer"]] = relationship(
        back_populates="user", cascade="delete"
    )


class Role(CidBase):
    role: Mapped[str]
    users: Mapped[list["User"]] = relationship(
        secondary="user_role_association", back_populates="roles"
    )
