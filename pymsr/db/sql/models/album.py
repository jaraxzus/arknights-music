from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .song import Song
    from .artist import Artist
    from .tag import Tag
    from .offer_models import AlbumArtistOffer, AlbumTagOffer

from sqlalchemy.orm import Mapped, mapped_column, relationship

from .base import CidBase


class Album(CidBase):
    name: Mapped[str] = mapped_column(index=True)
    cover_url: Mapped[str]
    intro: Mapped[str]
    belong: Mapped[str]
    cover_de_url: Mapped[str]
    songs: Mapped[list["Song"]] = relationship(back_populates="album", cascade="delete")
    artists: Mapped[list["Artist"]] = relationship(
        back_populates="albums",
        secondary="album_artist_association",
    )
    tags: Mapped[list["Tag"]] = relationship(
        secondary="tag_album_association", back_populates="albums"
    )
    artist_offers: Mapped[list["AlbumArtistOffer"]] = relationship(
        back_populates="album", cascade="delete"
    )
    tag_offers: Mapped[list["AlbumTagOffer"]] = relationship(
        back_populates="album", cascade="delete"
    )
