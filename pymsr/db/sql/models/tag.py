from typing import TYPE_CHECKING

from sqlalchemy.orm import Mapped, mapped_column, relationship

if TYPE_CHECKING:
    from .song import Song
    from .album import Album
    from .artist import Artist

from .base import CidBase


class Tag(CidBase):
    tag: Mapped[str] = mapped_column(index=True, unique=True)
    albums: Mapped[list["Album"]] = relationship(
        secondary="tag_album_association", back_populates="tags"
    )
    songs: Mapped[list["Song"]] = relationship(
        secondary="tag_song_association", back_populates="tags"
    )
    artists: Mapped[list["Artist"]] = relationship(
        secondary="tag_artist_association", back_populates="tags"
    )
