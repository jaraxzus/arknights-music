from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column

from .base import CidBase


class TagAlbumAsociation(CidBase):
    __tablename__ = "tag_album_association"
    __table_args__ = (
        UniqueConstraint("tag_cid", "album_cid", name="idx_unique_tag_album"),
    )
    tag_cid: Mapped[int] = mapped_column(ForeignKey("tags.cid", ondelete="CASCADE"))
    album_cid: Mapped[int] = mapped_column(ForeignKey("albums.cid", ondelete="CASCADE"))
