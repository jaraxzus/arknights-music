from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, mapped_column

from .base import CidBase


class TagArtistAsociation(CidBase):
    __tablename__ = "tag_artist_association"
    __table_args__ = (
        UniqueConstraint("tag_cid", "artist_cid", name="idx_unique_tag_artist"),
    )
    tag_cid: Mapped[int] = mapped_column(ForeignKey("tags.cid", ondelete="CASCADE"))
    artist_cid: Mapped[int] = mapped_column(
        ForeignKey("artists.cid", ondelete="CASCADE")
    )
