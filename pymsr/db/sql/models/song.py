from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from .album import Album
    from .artist import Artist
    from .tag import Tag
    from .offer_models import SongArtistOffer, SongTagOffer

from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column, relationship

from .base import CidBase


class Song(CidBase):
    name: Mapped[str] = mapped_column(index=True)
    album_cid: Mapped[int] = mapped_column(ForeignKey("albums.cid", ondelete="CASCADE"))
    source_url: Mapped[str]
    lyric_url: Mapped[str | None]
    mv_url: Mapped[str | None]
    mv_cover_url: Mapped[str | None]

    album: Mapped["Album"] = relationship(back_populates="songs")
    artists: Mapped[list["Artist"]] = relationship(
        secondary="song_artist_association", back_populates="songs"
    )
    tags: Mapped[list["Tag"]] = relationship(
        secondary="tag_song_association", back_populates="songs"
    )
    artist_offers: Mapped[list["SongArtistOffer"]] = relationship(
        back_populates="song", cascade="delete"
    )
    tag_offers: Mapped[list["SongTagOffer"]] = relationship(
        back_populates="song", cascade="delete"
    )
