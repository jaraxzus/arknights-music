from abc import ABC, abstractmethod


class AbstractRepository(ABC):
    @abstractmethod
    def add(self, data):
        raise NotImplementedError

    @abstractmethod
    async def get(self, cid):
        raise NotImplementedError

    @abstractmethod
    async def partial_update(self, data):
        raise NotImplementedError

    @abstractmethod
    async def delete(self, cid):
        raise NotImplementedError


class AbstractAlbumRepository(AbstractRepository):
    @abstractmethod
    async def get_full_album(self, cid):
        raise NotImplementedError

    @abstractmethod
    async def add_all(self, values):
        raise NotImplementedError

    @abstractmethod
    async def get_list(self):
        raise NotImplementedError


class AbstractSongRepository(AbstractRepository):
    pass


class AbstractTagArtistReposityry(AbstractRepository):
    @abstractmethod
    async def get_list(self):
        raise NotImplementedError


class AbstracOfferRepository(ABC):
    @abstractmethod
    async def add(self, data):
        raise NotImplementedError

    @abstractmethod
    async def delete(self, cid):
        raise NotImplementedError
