from aiofiles.os import mkdir
from aiofiles.ospath import isdir


async def create_directory(path: str) -> None:
    """
    Создает папку, если она не существует.
    """

    if not await isdir(path):
        await mkdir(path)
