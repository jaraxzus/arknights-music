from pydantic import BaseModel, EmailStr
from pydantic_core import Url
from pydantic_settings import BaseSettings, SettingsConfigDict


class EnvSettings(BaseSettings):
    TELEGRAM_BOT_TOKEN: str
    POSTGRES_USER: str
    POSTGRES_PASSWORD: str
    POSTGRES_DATABASE: str
    POSTGRES_HOST: str
    POSTGRES_PORT: int
    JWT_SECRET: str
    REACT_APP_API_URL: str
    SMTP_HOSTNAME: str
    SMTP_USE_TLS: bool
    SMTP_PORT: int
    SMTP_USERNAME: EmailStr
    SMTP_PASSWORD: str
    REDIS_URL: str
    MAIN_URL: Url
    PROFILING_ENABLED: bool
    model_config = SettingsConfigDict(env_file="./.env", env_file_encoding="utf-8")


env_settings = EnvSettings()


class DbSettings(BaseModel):
    echo: bool = False
    url: str = f"postgresql+asyncpg://{env_settings.POSTGRES_USER}:{env_settings.POSTGRES_PASSWORD}@db:{env_settings.POSTGRES_PORT}/{env_settings.POSTGRES_DATABASE}"


class Settings(BaseSettings):
    api_v1_prefix: str = "/api/v1"
    db: DbSettings = DbSettings()
    env: EnvSettings = env_settings
    # db_echo: bool = True


settings = Settings()
