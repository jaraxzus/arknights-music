import enum


class Operation(enum.Enum):
    DELETE = "DELETE"
    ADD = "ADD"
