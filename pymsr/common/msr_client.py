import asyncio
from types import TracebackType
from typing import Type

import aiofiles
import tqdm
from aiohttp import ClientTimeout
from aiohttp.client import ClientSession, TCPConnector

from pymsr.schemas import (AlbumCamelCaseInput, AlbumDetail,
                           AlbumDetailCamelCaseInput, AlbumDetailFull,
                           AlbumInput, SongDetailCamelCaseInput,
                           SongDetailWithArtistsAndTags)

from .utils import create_directory


class MSRClient:
    """The client is built around the ClientSession from aiohttp,
    it is recommended to use it through a async context manager."""

    def __init__(self) -> None:
        self.client = ClientSession(
            "https://monster-siren.hypergryph.com", connector=TCPConnector(limit=40)
        )
        self._download_client = ClientSession(
            connector=TCPConnector(limit=5),
            timeout=ClientTimeout(total=3600, connect=3600),
            headers={
                "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0",
                "Accept": "audio/webm,audio/ogg,audio/wav,audio/*;q=0.9,application/ogg;q=0.7,video/*;q=0.6,*/*;q=0.5",
                "Accept-Language": "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3",
                "Accept-Encoding": "gzip, deflate, br, identity",
                # "Referer": "https://monster-siren.hypergryph.com/",
                # "Origin": "https://monster-siren.hypergryph.com",
                "DNT": "1",
                "Sec-Fetch-Dest": "audio",
                "Sec-Fetch-Mode": "cors",
                "Sec-Fetch-Site": "cross-site",
                "Range": "bytes=0-",
                "Connection": "keep-alive",
                # Requests doesn't support trailers
                "TE": "trailers",
            },
        )

    async def _get(self, url: str) -> list[dict]:
        async with self.client.get("/api/" + url) as resp:
            if resp.status == 200:
                body = await resp.json()
                return body["data"]
            else:
                raise Exception(f"Error get {url}\n{resp}")

    async def get_albums(self) -> list[AlbumInput]:
        row_albums = await self._get("albums")
        # print(row_albums)
        return [AlbumCamelCaseInput.model_validate(album) for album in row_albums]

    async def get_album(self, cid: int) -> AlbumDetail:
        response = await self._get(f"album/{cid}/detail")
        return AlbumDetailCamelCaseInput.model_validate(response)

    async def get_song(self, cid: int) -> SongDetailCamelCaseInput:
        response = await self._get(f"song/{cid}")
        return SongDetailCamelCaseInput.model_validate(response)

    async def download_cover(self, url: str, name: str, path: str):
        try:
            async with self._download_client.get(url) as response:
                if response.status >= 300:
                    raise Exception(response)

                file_size = int(response.headers["Content-Length"])
                with tqdm.tqdm(
                    total=file_size, unit="B", unit_scale=True
                ) as progress_bar:
                    progress_bar.set_description(f"{name}")
                    extension = url.split(".")[-1]
                    await create_directory(path)
                    async with aiofiles.open(f"{path}/{name}.{extension}", "wb") as f:
                        while True:
                            chunk = await response.content.read(8192)
                            if not chunk:
                                break
                            progress_bar.update(len(chunk))
                            await f.write(chunk)
        except Exception as ex:
            print(url)
            print(ex)

    async def download_album(self, album: AlbumDetailFull, path: str):
        """Download the album to the specified directory, in the directory
        a folder is created with the name of the album into which the
        tracks will be downloaded"""

        album_path = f"{path}/{album.name}"
        await create_directory(album_path)
        tasks = []
        for song in album.songs:
            tasks.append(self.download_song(song, album_path))
        tasks.append(self.download_cover(album.cover_url, "cover", album_path))
        tasks.append(self.download_cover(album.cover_de_url, "cover_de", album_path))

        await asyncio.gather(*tasks)

    async def download_song(
        self, song: SongDetailWithArtistsAndTags | SongDetailCamelCaseInput, path: str
    ):
        while True:
            try:
                async with self._download_client.get(song.source_url) as response:
                    if response.status >= 300:
                        raise Exception(response)
                    file_size = int(response.headers["Content-Length"])
                    with tqdm.tqdm(
                        total=file_size, unit="B", unit_scale=True
                    ) as progress_bar:
                        progress_bar.set_description(f"{song.name}")
                        await create_directory(path)
                        async with aiofiles.open(f"{path}/{song.file_name}", "wb") as f:
                            while True:
                                chunk = await response.content.read(8192)
                                if not chunk:
                                    break
                                progress_bar.update(len(chunk))
                                await f.write(chunk)
                    break
            except Exception as response:
                print("error download")
                print(response)

    def __enter__(self) -> None:
        raise TypeError("Use async with instead")

    async def __aenter__(self) -> "MSRClient":
        return self

    async def __aexit__(
        self,
        exc_type: Type[BaseException] | None,
        exc_val: BaseException | None,
        exc_tb: TracebackType | None,
    ) -> None:
        await self.client.close()
        await self._download_client.close()
