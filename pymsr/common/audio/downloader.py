import asyncio

from pymsr.common.msr_client import MSRClient
from pymsr.schemas.album import AlbumDetail, AlbumDetailFull
from pymsr.schemas.song import SongDetailCamelCaseInput, SongDetailWithArtistsAndTags


async def get_album(album: AlbumDetail, msr: MSRClient) -> AlbumDetailFull:
    full_album = AlbumDetailFull(
        tags=[],
        artists=[],
        name=album.name,
        cover_url=album.cover_url,
        intro=album.intro,
        belong=album.belong,
        cover_de_url=album.cover_de_url,
        cid=album.cid,
        songs=[],
    )
    tasks = []
    for song in album.songs:
        tasks.append(msr.get_song(song.cid))
    result: list[SongDetailCamelCaseInput] = await asyncio.gather(*tasks)
    full_album.songs = [
        SongDetailWithArtistsAndTags(
            name=song.name,
            album_cid=song.album_cid,
            source_url=song.source_url,
            lyric_url=song.lyric_url,
            mv_url=song.mv_url,
            mv_cover_url=song.mv_cover_url,
            cid=song.cid,
            artists=[],
            tags=[],
        )
        for song in result
    ]
    return full_album


async def downlowad_msr():
    async with MSRClient() as msr:
        msr_albums = await msr.get_albums()

        for album in msr_albums:
            await msr.download_cover(
                album.cover_url, "cover", f"/home/data/Music/FMusic/msr/{album.name}"
            )
        deatailed_albums: list[AlbumDetail] = await asyncio.gather(
            *[msr.get_album(album.cid) for album in msr_albums]
        )
        full_albums = await asyncio.gather(
            *[get_album(album, msr) for album in deatailed_albums]
        )
        await asyncio.gather(
            *[
                msr.download_album(album, "/home/data/Music/FMusic/msr")
                for album in full_albums
            ]
        )
