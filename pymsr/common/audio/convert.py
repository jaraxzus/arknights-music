import os
import shutil

import pydub
from mutagen.flac import FLAC, Picture
from mutagen.id3 import _specs
from tqdm import tqdm


def attach_metadata(flac_file_path: str, cover_path: str):
    """
    Прикрепляет данные о создателе, название трека и иконку к .flac файлу.

    Args:
        flac_file: Объект FLAC файла.
        cover_path: Путь к изображению.

    Returns:
        None.
    """

    flac_file = FLAC(flac_file_path)
    # Прикрепляем изображение.
    pic = Picture()
    with open(cover_path, "rb") as f:
        pic.data = f.read()

    pic.type = _specs.PictureType.COVER_FRONT
    pic.mime = "image/jpeg"
    pic.width = 500
    pic.height = 500
    pic.depth = 16  # color depth
    flac_file.add_picture(pic)
    flac_file.save()


def convert_wav_to_flac(wav_file_path: str, output_folder: str):
    base_name, _ = os.path.splitext(os.path.basename(wav_file_path))
    flac_file_path = os.path.join(output_folder, f"{base_name}.flac")

    # Проверка на существование файла
    if not os.path.exists(flac_file_path):
        wav_file = pydub.AudioSegment.from_file(wav_file_path)
        wav_file.export(flac_file_path, format="flac")
        cover_path = os.path.join(output_folder, "cover.jpg")
        if os.path.exists(cover_path):
            attach_metadata(flac_file_path, os.path.join(output_folder, cover_path))
        else:
            attach_metadata(
                flac_file_path,
                os.path.join(output_folder, os.path.join(output_folder, "cover.png")),
            )

    return flac_file_path


def process_playlist(playlist_folder: str):
    for root, _, files in os.walk(playlist_folder):
        for file in tqdm(files, desc=f"Processing {playlist_folder}"):
            if file.lower().endswith(".wav"):
                wav_file_path = os.path.join(root, file)
                try:
                    convert_wav_to_flac(wav_file_path, root)
                except Exception as ex:
                    print(
                        f"error when process: {playlist_folder}\nfile: {wav_file_path}\nerror: {ex}"
                    )


def search_mp3(playlist_folder: str):
    for root, _, files in os.walk(playlist_folder):
        for file in files:
            if file.lower().endswith(".mp3"):
                print(file)


def delete_wav_files(playlist_folder: str):
    for root, _, files in os.walk(playlist_folder):
        for file in files:
            if file.lower().endswith(".wav"):
                file_path = os.path.join(root, file)
                os.remove(file_path)
                print(f"Deleted {file}")


def move_flac_to_mp3(playlist_folder: str, flac_folder: str):
    for root, _, files in os.walk(playlist_folder):
        for file in files:
            if file.lower().endswith(".mp3"):
                mp3_path = os.path.join(root, file)
                flac_file = file[0:-4] + ".flac"

                if os.path.exists(os.path.join(playlist_folder, flac_file)):
                    continue

                flac_path = os.path.join(flac_folder, flac_file)

                if os.path.exists(flac_path):
                    new_flac_path = os.path.join(root, flac_file)
                    shutil.move(flac_path, new_flac_path)
                    print(f"Moved {flac_file} next to {file}")
                else:
                    print(f"Not found flac ver for track {file} on {playlist_folder}")


def process_folder(main_folder: str):
    for playlist_folder in os.listdir(main_folder):
        playlist_folder_path = os.path.join(main_folder, playlist_folder)
        if os.path.isdir(playlist_folder_path):
            # process_playlist(playlist_folder_path)
            # search_mp3(playlist_folder_path)
            # move_flac_to_mp3(playlist_folder_path, "/home/data/Music/Arknights/row/")
            delete_wav_files(playlist_folder_path)


if __name__ == "__main__":
    playlist_folder = "/home/fast/Media/Music/msr/叙拉古人OST_test"
    process_playlist(playlist_folder)
