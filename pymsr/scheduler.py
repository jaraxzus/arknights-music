import asyncio
import datetime

from pymsr.db.sql.repositories.unitofwork import UnitOfWork
from pymsr.services import SongsService, UpdateService


async def update_db():
    while True:
        try:
            update_service = UpdateService()
            song = await SongsService().get(514584, UnitOfWork())

            timestamp_hex = song.source_url.split("/")[-5]
            timestamp = int(timestamp_hex, 16)

            # Преобразование числа времени в объект datetime
            date_time = datetime.datetime.fromtimestamp(timestamp)

            # Расчет времени ожидания
            time_to_wait = (date_time - datetime.datetime.utcnow()).total_seconds() + 10

            # Если время в прошлом, не ждем
            if time_to_wait < 0:
                # print(f"Waiting for {time_to_wait} seconds...")
                await update_service.update()
                await update_service.update_sounds_url()
            await asyncio.sleep(20)
        except Exception as ex:
            print(ex)
