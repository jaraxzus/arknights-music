import os
import sys
from contextlib import asynccontextmanager

from pymsr.middleware import register_profiling_middleware
from pymsr.scheduler import update_db

current_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(current_dir)

import asyncio

import uvicorn
from api.v1.api_v1 import api_v1
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend
from redis import asyncio as aioredis

from pymsr.common.settings import settings

origins = ["http://localhost", "http://localhost:8080", "http://localhost:3000"]


@asynccontextmanager
async def lifespan(app: FastAPI):
    redis = aioredis.from_url(
        settings.env.REDIS_URL, encoding="utf8", decode_responses=True
    )
    FastAPICache.init(RedisBackend(redis), prefix="fastapi-cache")
    loop = asyncio.get_event_loop()
    loop.create_task(update_db())
    yield


app = FastAPI(lifespan=lifespan, docs_url="/api/v1/docs", redoc_url=None)
app.include_router(api_v1)


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
register_profiling_middleware(app)


def main():
    uvicorn.run("pymsr.main:app", host="localhost", port=8080, reload=True)


if __name__ == "__main__":
    main()
