import asyncio

from pymsr.services import UpdateService

if __name__ == "__main__":
    asyncio.run(UpdateService().update())
