import asyncio
import contextlib
import os
from typing import AsyncGenerator

import pytest
from fastapi.testclient import TestClient
from fastapi_cache import FastAPICache
from fastapi_cache.backends.inmemory import InMemoryBackend
from httpx import AsyncClient

# from pymsr.auth.schemas import UserCreate
from pymsr.auth.users import (
    UserManager,
    current_super_user,
    get_user_db,
    get_user_manager,
)
from pymsr.common.settings import settings
from pymsr.db.db_helper import db_helper
from pymsr.db.sql.models import (
    Album,
    AlbumArtistAsociation,
    Artist,
    Base,
    Song,
    SongArtistAsociation,
    Tag,
    TagAlbumAsociation,
    TagSongAsociation,
    User,
)

# from pymsr.db.sql.models import Base
from pymsr.main import app

# from pytest_asyncio.plugin import event_loop


metadata = Base.metadata


metadata.bind = db_helper.engine

app.dependency_overrides[db_helper.get_session] = db_helper.get_session
# print(app.dependency_overrides)
# app.dependency_overrides[
#
# ] = None


async def mock_super_user():
    pass


# @pytest.fixture(scope="session", autouse=True)
# def app():
#     # Устанавливаем переменные окружения для тестовой базы данных
#     os.environ["DB_ENV"] = "db_url"
#
#     # Создаем экземпляр приложения
#     app_instance = app
#
#     # Возвращаем экземпляр приложения
#     yield app_instance
#
#     # Завершаем тестирование и сбрасываем переменные окружения после завершения сессии
#     os.environ.pop("DB_ENV", None)


app.dependency_overrides[current_super_user] = mock_super_user
get_async_session_context = contextlib.asynccontextmanager(db_helper.session_dependency)


@pytest.fixture(autouse=True)
def _init_cache():
    FastAPICache.init(InMemoryBackend())
    yield
    FastAPICache.reset()


# get_user_db_context = contextlib.asynccontextmanager(get_user_db)
# get_user_manager_context = contextlib.asynccontextmanager(get_user_manager)


# SETUP
@pytest.fixture(scope="session", autouse=True)
def event_loop(request):
    """Create an instance of the default event loop for each test case."""
    loop = asyncio.get_event_loop_policy().new_event_loop()
    yield loop
    loop.close()


async def create_db():
    async with db_helper.engine.begin() as conn:
        await conn.run_sync(metadata.create_all)


async def delete_db():
    async with db_helper.engine.begin() as conn:
        await conn.run_sync(metadata.drop_all)


# @pytest.fixture(scope="session", autouse=True)
# async def setup_db():
#     print(f"{settings.db.url=}")
#     # assert settings.MODE == "TEST"
#     # await create_db()
#     # Base.metadata.create_all(engine)


@pytest.fixture(autouse=True, scope="session")
async def prepare_database(event_loop):
    print(f"{settings.db.url=}")
    try:
        await create_db()
        print("создание бд")
        yield
    finally:
        await delete_db()
        pass


@pytest.fixture(scope="session")
async def album_cid(ac: AsyncClient):
    data = {
        "name": "test_album_relation",
        "cover_url": "string",
        "intro": "string",
        "belong": "string",
        "cover_de_url": "string",
    }
    response = await ac.post("albums/", json=data)
    assert response.status_code == 201
    album = response.json()
    return album["cid"]


@pytest.fixture(scope="session")
async def tag_cid(ac: AsyncClient):
    data = {"tag": "fixtured tag"}
    response = await ac.post("tags/", json=data)
    assert response.status_code == 201
    tag = response.json()
    return tag["cid"]


@pytest.fixture(scope="session")
async def artist_cid(ac: AsyncClient):
    data = {"name": "test add artist"}
    response = await ac.post("artists/", json=data)
    assert response.status_code == 201
    artist = response.json()
    return artist["cid"]


@pytest.fixture(scope="session")
async def song_cid(ac: AsyncClient, album_cid: int):
    data = {
        "name": "fixtured_song",
        "album_cid": album_cid,
        "source_url": "string",
        "lyric_url": "string",
        "mv_url": "string",
        "mv_cover_url": "string",
    }
    response = await ac.post(
        "song/",
        json=data,
    )
    song = response.json()
    assert response.status_code == 201
    assert song["name"] == data["name"]
    return int(song["cid"])


@pytest.fixture(scope="session", autouse=True)
async def create_album(prepare_database):
    async with db_helper.session() as session:
        album = [
            Album(
                cid=2,
                name="test_album",
                cover_url="string",
                intro="string",
                belong="string",
                cover_de_url="string",
            ),
            Album(
                cid=4,
                name="test_album_song",
                cover_url="fskldjf",
                intro="string",
                belong="string",
                cover_de_url="string",
            ),
        ]
        session.add_all(album)
        await session.flush()
        await session.commit()


@pytest.fixture(scope="session", autouse=True)
async def create_song(prepare_database, create_album):
    async with db_helper.session() as session:
        song = Song(
            cid=2,
            name="test_song",
            album_cid=2,
            source_url="string",
            lyric_url="string",
            mv_url="string",
            mv_cover_url="string",
        )
        session.add(song)
        await session.flush()
        await session.commit()


@pytest.fixture(scope="session", autouse=True)
async def create_artist(prepare_database, event_loop):
    async with db_helper.session() as session:
        artist = Artist(name="test_artist")
        session.add(artist)
        await session.flush()
        await session.commit()
        print(f"artist created {artist.name}, cid: {artist.cid}")


@pytest.fixture(scope="session", autouse=True)
async def create_tag(prepare_database):
    async with db_helper.session() as session:
        tag = Tag(tag="test_tag")
        session.add(tag)
        await session.flush()
        await session.commit()


@pytest.fixture(scope="session", autouse=True)
async def album_artist(prepare_database, create_album, create_artist):
    async with db_helper.session() as session:
        model = AlbumArtistAsociation(album_cid=2, artist_cid=1)
        session.add(model)
        await session.flush()
        await session.commit()


@pytest.fixture(scope="session", autouse=True)
async def album_tag(prepare_database, create_album, create_tag):
    async with db_helper.session() as session:
        model = TagAlbumAsociation(album_cid=2, tag_cid=1)
        session.add(model)
        await session.flush()
        await session.commit()


@pytest.fixture(scope="session", autouse=True)
async def song_artist(prepare_database, create_song, create_artist):
    async with db_helper.session() as session:
        model = SongArtistAsociation(song_cid=2, artist_cid=1)
        session.add(model)
        await session.flush()
        await session.commit()


@pytest.fixture(scope="session", autouse=True)
async def song_tag(prepare_database, create_song, create_tag):
    async with db_helper.session() as session:
        model = TagSongAsociation(song_cid=2, tag_cid=1)
        session.add(model)
        await session.flush()
        await session.commit()


@pytest.fixture(scope="session")
def client(app):
    return TestClient(app=app, base_url="http://test/api/v1")


@pytest.fixture(scope="session")
async def ac(
    event_loop,
    prepare_database,
    create_album,
    create_tag,
    create_song,
    create_artist,
    song_artist,
    album_tag,
    album_artist,
    song_tag,
) -> AsyncGenerator[AsyncClient, None]:
    async with AsyncClient(app=app, base_url="http://test/api/v1") as ac:
        yield ac
