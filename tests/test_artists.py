import pytest
from fastapi.testclient import TestClient
from httpx import AsyncClient


# @pytest.mark.asyncio
class TestAsyncArtists:
    async def test_add_artist(self, ac: AsyncClient):
        data = {"name": "test add artist"}
        response = await ac.post(
            "artists/",
            json=data,
        )
        assert response.status_code == 201
        assert response.json()["name"] == data["name"]
        data = {"pricol": "test"}
        response = await ac.post(
            "artists/",
            json=data,
        )
        assert response.status_code == 422

    async def test_get_artists(self, ac: AsyncClient):
        response = await ac.get("artists/")
        assert response.status_code == 200
        # print(response.json())
        # assert len(response.json()) == 1

    async def test_get_artist(self, ac: AsyncClient):
        response = await ac.get(f"/artists/2/")
        assert response.status_code == 200
        assert response.json()["name"] == "test add artist"
        response = await ac.get(f"/artists/9999999/")
        assert response.status_code == 404

    # async def test_putch_artist(self, ac: AsyncClient):
    #     name = "patched_artist"
    #     data = {"name": name}
    #     response = await ac.patch(
    #         f"artists/2/",
    #         json=data,
    #     )
    #     assert response.status_code == 200
    #     assert response.json()["name"] == name

    async def test_delete_artist(self, ac: AsyncClient):
        response = await ac.delete("artists/2/")
        assert response.status_code == 200

        response = await ac.delete("artists/2/")
        assert response.status_code == 404
