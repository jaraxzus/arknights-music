import pytest
from fastapi.testclient import TestClient
from httpx import AsyncClient


# @pytest.mark.asyncio
class TestAsyncSongs:
    async def test_add_song(self, ac: AsyncClient, album_cid: int):
        data = {
            "name": "test_song_post",
            "album_cid": album_cid,
            "source_url": "string",
            "lyric_url": "string",
            "mv_url": "string",
            "mv_cover_url": "string",
        }
        response = await ac.post(
            "song/",
            json=data,
        )
        song = response.json()
        assert response.status_code == 201
        assert song["name"] == data["name"]
        song_cid = int(song["cid"])
        print(song_cid)
        data = {"pricol": "test"}
        response = await ac.post(
            "song/",
            json=data,
        )
        assert response.status_code == 422

    async def test_get_song(self, ac: AsyncClient, song_cid: int):
        response = await ac.get(f"/song/{song_cid}/")
        assert response.status_code == 200
        assert response.json()["name"] == "fixtured_song"
        response = await ac.get(f"/song/9999999/")
        assert response.status_code == 404

    # async def test_putch_song(self, ac: AsyncClient):
    #     name = "patched_song"
    #     data = {"name": name}
    #     response = await ac.patch(
    #         f"songs/2/",
    #         json=data,
    #     )
    #     assert response.status_code == 200
    #     assert response.json()["name"] == name

    async def test_delete_song(self, ac: AsyncClient, song_cid: int):
        response = await ac.delete(f"song/{song_cid}/")
        assert response.status_code == 200

        response = await ac.delete(f"song/{song_cid}/")
        assert response.status_code == 404
