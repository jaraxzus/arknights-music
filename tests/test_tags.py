import pytest
from fastapi.testclient import TestClient
from httpx import AsyncClient


# @pytest.mark.asyncio
class TestAsyncTags:
    async def test_add_tag(self, ac: AsyncClient):
        data = {"tag": "test add tag"}
        response = await ac.post(
            "tags/",
            json=data,
        )
        assert response.status_code == 201
        assert response.json()["tag"] == data["tag"]
        data = {"pricol": "test"}
        response = await ac.post(
            "tags/",
            json=data,
        )
        assert response.status_code == 422

    async def test_get_tags(self, ac: AsyncClient):
        response = await ac.get("tags/")
        assert response.status_code == 200
        # print(response.json())
        # assert len(response.json()) == 1

    async def test_get_tag(self, ac: AsyncClient, tag_cid: int):
        response = await ac.get(f"/tags/{tag_cid}/")
        assert response.status_code == 200
        assert response.json()["tag"] == "fixtured tag"
        response = await ac.get(f"/tags/9999999/")
        assert response.status_code == 404

    # async def test_putch_tag(self, ac: AsyncClient):
    #     name = "patched_tag"
    #     data = {"name": name}
    #     response = await ac.patch(
    #         f"tags/2/",
    #         json=data,
    #     )
    #     assert response.status_code == 200
    #     assert response.json()["name"] == name

    async def test_delete_tag(self, ac: AsyncClient):
        response = await ac.delete("tags/2/")
        assert response.status_code == 200

        response = await ac.delete("tags/2/")
        assert response.status_code == 404
