import pytest
from httpx import AsyncClient


@pytest.mark.asyncio
class TestAlbumsTagsAndArtistsRelations:
    async def test_create_album_artist_relation(
        self, ac: AsyncClient, album_cid: int, artist_cid: int
    ):
        data = {
            "album_cid": 999999,
            "artist_cid": artist_cid,
        }
        response = await ac.post("/relations/album-artist/", json=data)
        assert response.status_code == 404

        data = {
            "album_cid": album_cid,
            "artist_cid": artist_cid,
        }
        print(data)
        response = await ac.post("/relations/album-artist/", json=data)
        print(response.json())
        assert response.status_code == 201
        a_a_relation = response.json()
        assert a_a_relation["album_cid"] == data["album_cid"]
        assert a_a_relation["artist_cid"] == data["artist_cid"]

        data = {
            "artist_cid": artist_cid,
        }
        response = await ac.post("/relations/album-artist/", json=data)
        assert response.status_code == 422

    # async def test_delete_song_tag_relation(
    #     self, ac: AsyncClient, album_cid: int, artist_cid: int
    # ):
    #     data = {
    #         "album_cid": 999999,
    #         "artist_cid": artist_cid,
    #     }
    #     response = await ac.delete("/relations/album-artist/", json=data)
    #     assert response.status_code == 404
    #
    #     data = {
    #         "album_cid": album_cid,
    #         "artist_cid": artist_cid,
    #     }
    #     print(data)
    #     response = await ac.delete("/relations/album-artist/", json=data)
    #     print(response.json())
    #     assert response.status_code == 200
    #     a_a_relation = response.json()
    #     assert a_a_relation["album_cid"] == data["album_cid"]
    #     assert a_a_relation["artist_cid"] == data["artist_cid"]
    #
    #     data = {
    #         "artist_cid": artist_cid,
    #     }
    #     response = await ac.delete("/relations/album-artist/", json=data)
    #     assert response.status_code == 404

    async def test_create_album_tag_relation(
        self, ac: AsyncClient, album_cid: int, tag_cid: int
    ):
        data = {
            "album_cid": 999999,
            "tag_cid": tag_cid,
        }
        response = await ac.post("/relations/album-tag/", json=data)
        assert response.status_code == 404

        data = {
            "album_cid": album_cid,
            "tag_cid": tag_cid,
        }
        print(data)
        response = await ac.post("/relations/album-tag/", json=data)
        print(response.json())
        assert response.status_code == 201
        a_a_relation = response.json()
        assert a_a_relation["album_cid"] == data["album_cid"]
        assert a_a_relation["tag_cid"] == data["tag_cid"]

        data = {
            "tag_cid": tag_cid,
        }
        response = await ac.post("/relations/album-tag/", json=data)
        assert response.status_code == 422
