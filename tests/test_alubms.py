from httpx import AsyncClient


class TestAsyncAlbums:
    async def test_add_albums(self, ac: AsyncClient):
        data = {
            "name": "test_album",
            "cover_url": "string",
            "intro": "string",
            "belong": "string",
            "cover_de_url": "string",
        }
        response = await ac.post(
            "albums/",
            json=data,
        )
        assert response.status_code == 201
        assert response.json()["name"] == data["name"]
        data = {
            "cover_url": "string",
            "intro": "string",
            "belong": "string",
            "cover_de_url": "string",
        }
        response = await ac.post(
            "albums/",
            json=data,
        )
        assert response.status_code == 422

    async def test_get_albums(self, ac: AsyncClient):
        response = await ac.get("albums/")
        assert response.status_code == 200
        # data: list = response.json()
        # assert len(data) == 2

    async def test_get_album(self, ac: AsyncClient):
        response = await ac.get("albums/")
        assert response.status_code == 200
        data: list = response.json()
        response = await ac.get(f"/albums/{data[0]['cid']}/")
        assert response.status_code == 200
        assert response.json()["name"] == "test_album"
        response = await ac.get(f"/albums/9999999/")
        assert response.status_code == 404

    async def test_putch_album(self, ac: AsyncClient):
        name = "patched_album"
        data = {"name": name}
        response = await ac.patch(
            f"albums/2/",
            json=data,
        )
        assert response.status_code == 200
        assert response.json()["name"] == name

    async def test_delete_alubm(self, ac: AsyncClient):
        response = await ac.delete("albums/2/")
        assert response.status_code == 200

        response = await ac.delete("albums/2/")
        assert response.status_code == 404
