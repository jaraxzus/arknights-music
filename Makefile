# Makefile
.PHONY: test docker-dev-up docker-dev-down docker-prod-up docker-dev-down

# Определение переменных
DOCKER_COMPOSE_DEV = ./docker-compose/docker-compose.dev.yml
DOCKER_COMPOSE_DEV_WITH_FRONT = ./docker-compose/docker-compose.dev.with-front.yml
DOCKER_COMPOSE_PROD = ./docker-compose/docker-compose.prod.yml
DOCKER_COMPOSE_PROD_WITH_FRONT = ./docker-compose/docker-compose.prod.with-front.yml

# Команда для запуска тестов (использовать в терминале докера!!!)
# test:
	# python manage.py test
# Команда для запуска сервера Dev режиме (использовать в терминале докера!!!)
run:
	python pymsr/main.py
# Команды для запуска Docker Compose в режиме разработки
# d - docker, d - dev, b - build, u - up, d - down
ddb:
	docker-compose -f $(DOCKER_COMPOSE_DEV) build
	
ddu:
	docker-compose -f $(DOCKER_COMPOSE_DEV) up -d

ddd:
	docker-compose -f $(DOCKER_COMPOSE_DEV) down

# Команды для запуска Docker Compose в режиме разработки c фронтом
ddfb:
	docker-compose -f $(DOCKER_COMPOSE_DEV_WITH_FRONT) build
	
ddfu:
	docker-compose -f $(DOCKER_COMPOSE_DEV_WITH_FRONT) up -d

ddfd:
	docker-compose -f $(DOCKER_COMPOSE_DEV_WITH_FRONT) down


# Команды для запуска Docker Compose в режиме продакшн
# d - docker, p - prod, b - build, u - up, d - down
dpb:
	docker-compose -f $(DOCKER_COMPOSE_PROD) build
	
dpu:
	docker-compose -f $(DOCKER_COMPOSE_PROD) up -d

dpd:
	docker-compose -f $(DOCKER_COMPOSE_PROD) down

# Команды для запуска с фронтом
dpfb:
	docker-compose -f $(DOCKER_COMPOSE_PROD_WITH_FRONT) build
	
dpfu:
	docker-compose -f $(DOCKER_COMPOSE_PROD_WITH_FRONT) up -d

dpfd:
	docker-compose -f $(DOCKER_COMPOSE_PROD_WITH_FRONT) down
