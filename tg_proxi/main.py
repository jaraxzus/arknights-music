import asyncio
import logging

from aiogram import Bot
from aiogram.dispatcher.dispatcher import Dispatcher
from aiogram.filters import Command, CommandStart

from tg_proxi.core.handlers.basic import search, start
from tg_proxi.core.settings import settings
from tg_proxi.core.utils.commands import set_commands


async def main():
    logging.basicConfig(level=logging.DEBUG)
    bot = Bot(settings.telegram_bot_token)

    dp = Dispatcher()
    dp.message.register(search, Command(commands="search"))
    dp.message.register(start, CommandStart)
    # with bot as sessoin:
    try:
        await dp.start_polling(bot)
        await set_commands(bot)
    finally:
        await bot.session.close()


if __name__ == "__main__":
    asyncio.run(main())
