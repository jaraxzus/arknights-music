from aiogram import Bot
from aiogram.types import BotCommand, BotCommandScopeDefault


async def set_commands(bot: Bot):
    commands = [
        BotCommand(command="start", description="Start working"),
        BotCommand(command="search", description="Search song with word"),
    ]
    await bot.set_my_commands(commands, BotCommandScopeDefault())
